#ifndef TRANSFER_H
#define TRANSFER_H

#include <vector>
#include "stat.hpp"
#include "Mesh.hpp"

class Transfer {
    public:
        virtual void move(Mesh& mesh) = 0;
    protected:
        /* must be called inside move() */
        virtual void markAsMelted(Mesh& mesh) = 0;
        Stat* stat;
};

#endif /*TRANSFER_H*/
