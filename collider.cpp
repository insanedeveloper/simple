#include "Mesh.hpp"
#include "mesh/unstruct/Polygon.hpp"
#include "collider.hpp"
#include "stat.hpp"

void Collider::collide(Mesh& mesh){
    for(size_t i = 0; i < mesh.getMyCells().size(); i++){
        Polygon* cell = mesh.getMyCells()[i];
        if(!cell->isFrozen()){
            collideWithin(cell, mesh.getMyCellIndexes()[i], mesh.getTimeStep());
        }
    }
}

void Collider::collideWithin(Polygon* cell, int cellID, double timeStep){
	Material* material = cell->getMaterial();
	if(!material){
		return;
	}
	Momentum& f = cell->f();
	Momentum& g = cell->g();

	//double before = f.getFlow().totalFlow();
	material->applyReactions(timeStep * cell->getDensity(), f, g);
	//double after = f.getFlow().totalFlow();
	//if(before != after){
	//	LOGTHROTTLE(ERROR, "flowerr", 1) << "Flow delta " << (after - before) << " != 0";
	//}
}
