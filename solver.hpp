#ifndef _SOLVER_HPP_
#define _SOLVER_HPP_

#include <limits>

#include "Throttle.hpp"

#include "logger/logger.hpp"

/*#include "Constructors.hpp"*/

#include "mesh/unstruct/UnstructMesh.hpp"
#include "Mesh.hpp"

#include "materials/material.hpp"
#include "source.hpp"
#include "Transfer.hpp"
#include "Transfer1.hpp"
#include "collider.hpp"
/*
#include "Transfer2.hpp"

#include "Integral.hpp"
*/
#include "stat.hpp"
#include <cmath>
#include <ctime>
#include <sys/time.h>

string formatInterval(long seconds){
    long minutes = seconds/60;
    stringstream str;
    if(minutes){
        seconds %= 60;
        long hours = minutes/60;
        if(hours){
            minutes %= 60;
            str << hours << "h ";
        }
        str << minutes << "m ";
    }
    str << seconds << "s";
    return str.str();
}

void logfunc(double delta, double firstDelta, double targetDelta, int begin, string rest){
    long now;
    {
        timeval tv;
        gettimeofday(&tv, NULL);
        now = tv.tv_sec;
    }
    long finishtime = 0;
    long dt = now - begin;

    if(delta && firstDelta && delta != firstDelta){
        finishtime = (double)dt*log(targetDelta/firstDelta)/log(delta/firstDelta);
    }
    long remainsSec = 0;

    stringstream str;

    if(finishtime){
        str <<"" << 100*dt/finishtime << "%: ";
        remainsSec = finishtime - dt;
    }
    str << formatInterval(dt);
    if(finishtime){
        str << " of " << formatInterval(finishtime);
    }
    if(remainsSec){
        str << ", remains " << formatInterval(remainsSec);
    }
    LOG(INFO) << "[" << str.str() << "] " << rest;
}

string normalizePath(string base, string path){
    if(path[0] == '/'){
        return path;
    }
    return base + path;
}

void normalizePaths(Json::Value& manifest, string path){
    Json::Value& stat = manifest["stat"]["file"];
    manifest["stat"]["file"] = Json::Value(normalizePath(path, stat.asString()));
    Json::Value& mesh = manifest["mesh"];
    manifest["mesh"] = Json::Value(normalizePath(path, mesh.asString()));
    Json::Value& materialsFile = manifest["materialsFile"];
    manifest["materialsFile"] = Json::Value(normalizePath(path, materialsFile.asString()));
}



template <typename MeshType>
class Solver {
    public:
        Solver(Json::Value manifest, const string path) {
            long beginTime;
            {
                timeval tv;
                gettimeofday(&tv, NULL);
                beginTime = tv.tv_sec;
            }

            normalizePaths(manifest, path);

        	int vCount = manifest["vCount"].asInt(),
        		gCount = manifest["gCount"].asInt();

        	double minEnergy = manifest["minEnergy"].asDouble(),
        	    maxEnergy = manifest["maxEnergy"].asDouble();

        	scaleType energyScale = manifest["logScale"].asBool()? LOGARITHMIC : LINEAR;

        	double courant = manifest["courantNumber"].asDouble();
        	if(vCount){
        		Momentum::resizeSpaceTo(vCount);
        	}else{
        		Momentum::resizeSpaceTo(manifest["vectors"]);
        	}

        	EnergyDistribution::setResolution(gCount, minEnergy, maxEnergy, energyScale);
        	EnergyDistribution::setCourantNumber(courant);

        	LOG(INFO) << "Resizing momentum space to "<< vCount << "x" << gCount <<
        			" (" << minEnergy << "MeV - " << maxEnergy << "MeV " <<
        			(energyScale == LINEAR? "linear":"logarithmic") << " scale)";
        	LOG(INFO) << '\n' << EnergyDistribution::getSpaceInfo();

        	Material::load(manifest["materialsFile"], manifest["materials"], manifest["reactions"]);

        	Source::load(manifest["sources"]);

            MeshBase* mesh_base_p = new UnstructMesh(manifest);
            Mesh*     mesh_p   = new MeshType(mesh_base_p);
            Mesh& mesh = *mesh_p;

            LOG(INFO) << "facets.size() = "    << mesh.getAllFacets().size() 
                      << " ffacets.size() = "  << mesh.getFlowingFacets().size();

            LOG(INFO) << "cells.size() = "     << mesh.getAllCells().size() 
                      << " fcells.size() = "   << mesh.getFlowingCells().size()
                      << " mycells.size() = "  << mesh.getMyCells().size();

            Transfer* transfer;
            int order = manifest["transfer"].isMember("order")?
                manifest["transfer"]["order"].asInt() : 1;
            LOG(INFO)<< "order = " << order;

            /*if (order == 2)
            {
                for (size_t i = 0; i < mesh.getFlowingCells().size(); ++i) {
                    mesh.getFlowingCells()[i]->f().giveMemoryToGradient();
                }
                transfer = new Transfer2(mesh);
            }
            else*/

            Stat stat(manifest["stat"]);

            transfer = new Transfer1(stat);
            Collider collider(stat, 0);

            Throttle *logi = new Throttle(logfunc, 1);

            double targetPrecision = manifest.isMember("targetPrecision") ?
                manifest["targetPrecision"].asDouble(): 1E-10;

            double firstDelta = 0;
            int i = 0;
            int notLastIter = 1;

            size_t cellsCount = mesh.getMyCells().size();

            for(size_t j = 0; j < cellsCount; j++){
               mesh.getMyCells()[j]->reset();
            }

            while(notLastIter){

                if(notLastIter == 2){
                    notLastIter = 0;
                    LOG(INFO)<<"Very last";
                    stat.last();
                }else{
                    stat.turnOn(i);
                }

                mesh.newStep();


                transfer->move(mesh);


                collider.collide(mesh);
                {
                    double maxDelta = mesh.getMaxDelta();
                    double minDelta = maxDelta/10;
                    Polygon::setMinDelta(minDelta);


                    size_t frozenCells = 0;
                    size_t meltedCells = 0;
                    for(size_t j = 0; j < cellsCount; j++){

                        Polygon* cell = mesh.getMyCells()[j];
                        int frozenState = 0;

                        if(cell->isFrozen()){
                            frozenState = 2;
                            frozenCells++;
                        }
                        if(cell->isMelted()){
                            frozenState = 1;
                            meltedCells++;
                        }
                        size_t id = mesh.getMyCellIndexes()[j];
                        stat.saveDouble(id, "frozen", frozenState);
                        double delta = cell->getDelta();
                        stat.saveDouble(id, "abs_delta", delta);
                        stat.saveDouble(id, "rel_delta", delta/maxDelta);
                    }


                    {
                        stringstream str;
                        if(!firstDelta){
                            firstDelta = maxDelta;
                        }
                        str << i;
                        logi->run(maxDelta, firstDelta, targetPrecision, beginTime, str.str());
                    }

                    if(notLastIter && maxDelta <= targetPrecision){
                        LOG(INFO) << "Last iteration " << i << ", delta " << maxDelta << " < " << targetPrecision;
                        notLastIter = 2;
                    }
                    LOGTHROTTLE(INFO, "frozen", 0) << "Frozen/melted/total: " << frozenCells << "/"
                        << meltedCells << "/"
                        << cellsCount << ", delta: " << minDelta << " - " << maxDelta;
                }
                mesh.endStep();

                stat.collectDataAndPrint(mesh);

                i++;
            }
            LOG(INFO) << "\7\7Done";
            delete transfer;
            delete mesh_p;
            delete mesh_base_p;
        }
};


#endif
