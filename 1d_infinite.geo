Point(1) = {0, 0, 0, 10.0};
Point(2) = {5, 0, 0, 10.0};

Point(4) = {0, 5, 0, 10.0};
Line(1) = {1, 4};
Line(2) = {4, 2};

Line(4) = {2, 1};
Line Loop(29) = {1, 2, 4};
Plane Surface(30) = {29};
before[] = Extrude {0,0,5}{
    Surface{30}; Recombine; Layers{4};
};
middle[] = Extrude{0,0,25}{
    Surface{47}; Recombine; Layers{20};
};
after[] = Extrude{0,0,5}{
    Surface{64}; Recombine; Layers{4};
};
Physical Surface('source') = {30};
Physical Surface('infinite') = {38,42,46,55,59,63,72,76,80};
Physical Volume('void') = {1,3};
Physical Volume('shield') = {2};

