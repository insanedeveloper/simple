#ifndef __MOMENT342__
#define __MOMENT342__

#include <vector>
#include "v.hpp"
#include "typedef.hpp"
#include "energyDistribution.hpp"
#include "logger/logger.hpp"
using namespace std;

class Momentum{
	friend class Cell;
public:
	Momentum();
	Momentum(const Momentum& src);
	~Momentum();
	static void resizeSpaceTo(size_t numVectors);
	/* vectors: массив массивов [x, y, z, ω] для каждого вектора */
	static void resizeSpaceTo(Json::Value vectors);
	static int findClosestFor(V& direction);
	static void findTripleFor(V& direction, int* indexes, double* fractions);

	void operator +=(const Momentum& d);
	void operator *=(const EnergyDistribution& factors);
	/**
	 * part в штуках, поэтому внутри этого оператора деление на dirWeights
	 */
	void operator +=(const EnergyDistribution& part);
	bool operator == (double value);
	bool isNaN();
	inline bool operator != (double value){
		return !(*this == value);
	}
	//Momentum& operator = (Momentum src);
	Momentum& operator = (Momentum& src);
	void clear();
	/**
	 * Полный скалярный поток. Поток, проинтегрированный по 4π
	 */
	EnergyDistribution getFlow();
	/**
	 * Скаляр. Произведение потока на косинус угла между dir и
	 * направлением потока, в полусфере dir
	 */
	EnergyDistribution getVectorFlowIn(V& dir);
	/**
	 * (Векторный) Ток. Векторный поток, проинтегрированный по 4π
	 */
	vector<V> getVectorFlow();

	/**
	 * Дамп всех потоков по векторам
	 */
	Json::Value toJson();

	inline static size_t getVectorCount(){
		return Momentum::vCount;
	}
	inline static double dotProduct(int i, V& v){
		return dot(Momentum::dirVectors[i], v);
	}
	inline EnergyDistribution& getGroupForVector(int i){
		return values[i];
	}
	inline static V& getVector(int i){
		return dirVectors[i];
	}
	inline static double getdSForVector(int i){
		return dirWeights[i];
	}
private:
	friend class MomentumTest;
	friend class PhysicalFacet;
	EnergyDistribution* values;

	static size_t vCount;
	static V* dirVectors;
	static double* dirWeights;

	static double capArea(double capAngle);
	static double capAngle(double area);

	static void staticFlush();
	friend void swap(Momentum& first, Momentum& second);
};
#endif
