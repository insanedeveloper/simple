#ifndef STAT34534__
#define STAT34534__

#include <json/json.h>
#include "Mesh.hpp"
enum Statprefs{
	STAT_TOTAL = 0x00000001,
	STAT_ELASTIC = 0x00000002,
	STAT_INELASTIC = 0x00000004,
	STAT_ABSORPTION = 0x00000008,
	STAT_ALLREACTIONS = 0x0000000F,
	STAT_ID = 0x00000010,
	STAT_DENSITY = 0x00000020,
	STAT_FLOW = 0x00000040,
	STAT_FLOWDIR = 0x00000080,
	STAT_ALLCELLDATA = 0x000000F0
};

class Stat{
public:
	Stat(Json::Value& data);
	void collectDataAndPrint(Mesh& mesh);
	void saveDouble(int cellID, std::string name, double value);
	void saveEnergyDistribution(int cellID, std::string name, EnergyDistribution& part);
	inline void turnOn(int i){
		if(i % rate == 0){
			snapshotI = i;
			data.clear();
		}
	}
	inline void last(){
	    snapshotI = -1;
	    data.clear();
	}

private:
	void printData(Mesh& mesh);
	Json::Value fluxToJson(vector<V> flux);
	inline void turnOff(){
		snapshotI = 0;
	}
	int rate;
	int snapshotI;
	std::vector<V> flowDirs;
	std::string file;
	Json::Value data;
};
#endif
