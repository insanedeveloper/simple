#ifndef __MATE4543__
#define __MATE4543__

#include <map>
#include <set>
#include "element.hpp"
#include "../logger/logger.hpp"
#include "../energyDistribution.hpp"
#include "../reactions/AbstractReaction.hpp"
#include "../reactions/Elastic.hpp"
#include "../reactions/Absorption.hpp"
#include "../reactions/Nonelastic.hpp"

#include <json/json.h>

typedef pair<int, int> za;

enum FractionType{
    UNSET,
    MASS,
    ATOMIC
};

#define NA 6.02214129E23
/* Сначала нужно создать экземпляры класса,
 * после чего вызывать load, который загрузит используемый набор элементов.
 * Чтобы получить групповые макроскопические сечения, нужно вызвать recalc в каждом экземпляре.
 * !Важно! Экземпляр создается не на каждую ячейку, а на каждый материал.
 * Т.е. материалы хранятся сбоку, а в ячейках присутствуют ссылки на нужный материал + плотность вещества.
 */
class Material{
public:

	~Material();
	static void load(Json::Value filename, Json::Value materialss, Json::Value reactions);
	static Material* getMaterialById(std::string id);
	static void recalcAll();
	//void applyCollision(Momentum& space, double timeStep, double density);

	inline std::string& getId(){
		return id;
	}
	void applyReactions(double mult, Momentum& src, Momentum& delta);
	EnergyDistribution total;
private:
	friend class MaterialTest;
	Material(std::string id, Json::Value list);
	void recalc();
	void checkReactions();
	AbstractReaction* getReaction(int mt);
	vector<AbstractReaction*> reactions;
	friend class Cell;
	std::string id;
	static set<za> usedElements;
	static map<za, Element> elements;
	static map<std::string, Material*> listOfMaterials;
	static void loadBigFile(string filename);
	static void disableReactions(Json::Value reactions);
	static bool isEnabled(int mt);
	static map<std::string,int> humanReadableNames;
	static set<int> disabled;
	/* Human readable names are stored as map, because of stupid */
	static void fillHRN();
	vector<pair<za, double> > cleanList;

	double normalize(double* fractions, size_t size);
};
#endif
