#include "material.hpp"
#include "humanReadableNames.hpp"
#include <fstream>
using namespace Json;

set<za> Material::usedElements;
map<za, Element> Material::elements;
map<std::string, Material*> Material::listOfMaterials;
map<std::string,int> Material::humanReadableNames;
set<int> Material::disabled;

void Material::load(Json::Value filename, Json::Value materials, Json::Value reactions){
	Material::fillHRN();
	Value::Members names = materials.getMemberNames();
	size_t count = names.size();
	for(size_t i = 0; i < count; i++){
		Material::listOfMaterials[names[i]] = new Material(names[i], materials[ names[i] ]);
	}
	Material::disableReactions(reactions["disabled"]);
	/* TODO: сделать имя файла опциональным параметром */
	Material::loadBigFile(filename.asString());
}

Material::Material(std::string id, Json::Value list){
    this->id = id;
    size_t count = list.size();
    za* ids = new za[count];
    double* fractions = new double[count];
    int* atomicWeights = new int[count];

    FractionType type = UNSET;
    for(Json::ArrayIndex i = 0; i < (Json::ArrayIndex)count; i++){
        Value& elem = list[i];
        int z = Material::humanReadableNames[elem["name"].asString()];
        int a = atomicWeights[i] = elem["weight"].asInt();
        ids[i] = za(z, a);
        if( (elem.isMember("massFraction") && type == ATOMIC) ||
            (elem.isMember("atomicFraction") && type == MASS)){
            LOG(ERROR)<< "Both atomic and mass fractions are used in material '" << id << "'";
        }
        if(elem.isMember("massFraction")){
            type = MASS;
            fractions[i] = elem["massFraction"].asDouble();
        }else if(elem.isMember("atomicFraction")){
            type = ATOMIC;
            fractions[i] = elem["atomicFraction"].asDouble();
        } else{
            fractions[i] = 0;
            LOG(ERROR) << "Neither mass fraction nor atomic fraction is defined for element '" << elem["name"].asString()
            << "' in material '" << id << "'";
        }
    }

    double sum = this->normalize(fractions, count);

    if(sum != 1){
        LOG(WARNING)<<"Warning: unnormalized fractions ("<<sum<<" total) of material '"
        << id << "'";
    }
    if(type == ATOMIC){
        for(size_t i = 0; i < count; i++){
            fractions[i] *= atomicWeights[i];
        }
        this->normalize(fractions, count);
    }


	for(size_t i = 0; i < count; i++){
        za current = ids[i];
        if(current.second == 0){
            LOG(ERROR)<<"Warning: natural mixture is not implemented, "<<
                "but atomic weight 0 is used";
        }
        Material::usedElements.insert(current);

        cleanList.push_back(pair<za, double>(current, fractions[i]));
    }
    delete[] fractions;
    delete[] atomicWeights;
    delete[] ids;
}
Material* Material::getMaterialById(std::string id){
	return Material::listOfMaterials[id];
}

Material::~Material(){
	map<std::string, Material*>::iterator ptr = Material::listOfMaterials.find(id);
	if(ptr != Material::listOfMaterials.end()){
		Material::listOfMaterials.erase(ptr);
	}
}
void Material::disableReactions(Json::Value disabled){
	if(disabled.size()){
		Material::disabled.clear();
		int l = disabled.size();
		for(int i = 0; i < l; i++){
			Material::disabled.insert(disabled[i].asInt());
		}
	}
}
bool Material::isEnabled(int mt){
	set<int>::iterator end = Material::disabled.end();
	set<int>::iterator ptr = Material::disabled.find(mt);
	return ptr == end;
}
void Material::applyReactions(double mult, Momentum& src, Momentum& delta){
	size_t count = reactions.size();
	for(size_t i = 0; i < count; i++){
		reactions[i]->apply(mult, src, delta);
	}
}
AbstractReaction* Material::getReaction(int mt){
	size_t count = reactions.size();
	for(size_t i = 0; i < count; i++){
		if(reactions[i]->getReactionMT() == mt){
			return reactions[i];
		}
	}
	AbstractReaction* reaction = 0;
	switch(mt){
		case REACTION_EL:
			reaction = new Elastic();
			break;
		case REACTION_ABS:
			reaction = new Absorption();
			break;
		case REACTION_EE:
			reaction = new Nonelastic();
			break;
		default:
			LOG(ERROR) << "Wtf reaction #" << mt;
	}
	reactions.push_back(reaction);
	return reaction;
}
/**
 * TODO: test
 */
void Material::recalc(){
	size_t elementCount = cleanList.size();
	EnergyDistribution nonelastic = 0;

	for(size_t i = 0; i < elementCount; i++ ){
		pair<za, double> fraction = cleanList[i];
		Element& elem = elements[fraction.first];
		double weight = elem.getWeight();
		double k = fraction.second * (NA * 1E-24)/weight; // 1E-24 - из барнов в см2
		map<int, EnergyDistribution>::iterator end = elem.crossSections.end();
		map<int, EnergyDistribution>::iterator ptr = elem.crossSections.begin();

		while(ptr != end){
			int mt = ptr->first;
			EnergyDistribution& crossSection = ptr->second;
			if(mt == REACTION_TOT){
				total.multAndAdd(crossSection, k);
			}else if(Material::isEnabled(mt)){
				AbstractReaction* reaction = this->getReaction(mt);
				if(!reaction){
					LOG(ERROR) << "Wtf no reaction";
				}
				reaction->add(crossSection*k, weight);
				nonelastic.multAndAdd(crossSection, k);
			}else{
				LOG(INFO) << "Reaction #" << mt << " is disabled";
			}
			ptr++;
		}
	}
	if(Material::isEnabled(REACTION_NONEL) && Material::isEnabled(REACTION_EE)){
		AbstractReaction* reaction = this->getReaction(REACTION_EE);
		reaction->add(total - nonelastic, 0);
	}else{
		LOG(INFO) << "Reaction #" << REACTION_EE << " is disabled";
	}
	this->checkReactions();
}
void Material::recalcAll(){
	map<std::string, Material*>::iterator end = Material::listOfMaterials.end();
	map<std::string, Material*>::iterator ptr = Material::listOfMaterials.begin();
	while(ptr != end){
		ptr->second->recalc();
		ptr++;
	}
}
void Material::checkReactions(){
	LOG(INFO) << "total\n" << total;
	size_t count = reactions.size();
	for(size_t i = 0; i < count; i++){
		if(reactions[i]->isNaN()){
			LOG(ERROR) << "NaN values in reaction " << reactions[i]->getReactionMT() << " of material '" << this->getId() << "'";
		}
		reactions[i]->log();
	}
}
void Material::loadBigFile(string filename){
	Value list;
	Reader reader;
	fstream file;
	LOG(INFO) << "Loading from '" << filename << "'";
	file.open(filename.c_str(), ios::in);
	bool parsingSuccessful = reader.parse( file, list );
	file.close();
	if ( !parsingSuccessful ){
	    // report to the user the failure and their locations in the document.
	    LOG(ERROR)  << "Failed to parse elements definitions from '" << filename << "'\n"
	               << reader.getFormatedErrorMessages();
	    return;
	}
	size_t len = list.size();
	set<za>::iterator notFound = Material::usedElements.end();
	for(Json::ArrayIndex i = 0; i < (Json::ArrayIndex)len; i++){
		Value node = list[i];
		za elemId(node["atomicNumber"].asInt(), node["massNumber"].asInt());
		set<za>::iterator setItem = Material::usedElements.find(elemId);
		if( setItem != notFound ){
			 LOG(INFO) << "Loading element " << elemId.first << ":" << elemId.second;
			 Material::usedElements.erase(setItem);
			 Material::elements.insert(pair<za,Element>(elemId, Element(node)));
		}
	}
	/* если тут не ноль, значит в файле не было хотя бы одного нужного элемента */
	if(Material::usedElements.size() != 0){
		LOG(ERROR) << "Failed to load some elements:";
		set<za>::iterator begin = Material::usedElements.begin();
		set<za>::iterator end = Material::usedElements.end();
		while(begin != end){
		    LOG(ERROR) << begin->first << ":" << begin->second;
		    begin++;
		}

	}
	LOG(INFO) << "Recalc crossSections";
	recalcAll();
};


double Material::normalize(double* fractions, size_t size){
    double sum = 0;
    for(size_t i = 0; i < size; i++){
        sum += fractions[i];
    }
    if(sum != 1){
        for(size_t i = 0; i < size; i++){
            fractions[i] /= sum;
        }
    }
    return sum;
}
