#include "element.hpp"
#include <iostream>
#include <stdlib.h>

using namespace Json;

Element::Element(){
	atomicNumber = massNumber = temperature = 0;
}

Element::Element(Json::Value elem){
    atomicNumber = elem["atomicNumber"].asInt();
	massNumber = elem["massNumber"].asInt();


	temperature = elem["temperature"].asDouble();
	weight = elem["weight"].asDouble();
	LOG(INFO) << "Loading elem " << atomicNumber << ":" << massNumber<<", T: " << temperature<<"K, weight:" <<weight;
	Value reactionsList = elem["reactions"];
	Value::Members keys = reactionsList.getMemberNames();
	size_t count = keys.size();
	for(size_t i = 0; i < count; i++){
		crossSections[atoi(keys[i].c_str())] = EnergyDistribution::toEnergyGroups(reactionsList[keys[i]]);
	}
}

EnergyDistribution& Element::getCrossSection(int number){
    map<int, EnergyDistribution>::iterator end = crossSections.end();
    map<int, EnergyDistribution>::iterator elem = crossSections.find(number);
    if(elem == end){
        LOG(ERROR)<< "No reaction №" << number;
        EnergyDistribution& fake = crossSections[number];
        fake = 0;
        return fake;
    }
    return elem->second;
}
