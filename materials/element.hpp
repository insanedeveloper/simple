#ifndef __ELEME3242__
#define __ELEME3242__

#include <json/json.h>
#include <vector>
#include <map>
#include <string>
#include "../energyDistribution.hpp"
#define REACTION_TOT 1 // total, общее
#define REACTION_EL 2 // elastic, упругое
#define REACTION_ABS 27 // absorption, поглощение
#define REACTION_NONEL 3 // Nonelastic, неупругое
#define REACTION_EE -1 // Everyting else, считаем это неупругим
using namespace std;

class Element {
public:
	Element(Json::Value elem);
	Element();
	EnergyDistribution& getCrossSection(int number);
	inline double getWeight(){
		return weight;
	}
	map<int,EnergyDistribution> crossSections;
protected:
	friend class ElementTest;
	int atomicNumber;
	int massNumber;
	double weight;
	double temperature;

};

#endif
