#include "MeshSingle.hpp"

MeshSingle::MeshSingle(MeshBase* mesh) :
    time_step(mesh->getTimeStep()),
    cells(mesh->getCells()),
    facets(mesh->getFacets())
{
    my_cell_indexes.reserve(cells.size());
    for (size_t i = 0; i < cells.size(); ++i) 
        my_cell_indexes.push_back(i);
}

void MeshSingle::newStep() {
	for (size_t i = 0; i < cells.size(); ++i) {
		cells[i]->newStep();
	}
}

void MeshSingle::endStep() {
	for (size_t i = 0; i < cells.size(); ++i) {
		cells[i]->iterate();
	}
}

double MeshSingle::getMaxDelta(){
    double delta = 0;
    for (size_t i = 0; i < cells.size(); ++i) {
        Polygon* cell = cells[i];
        delta = max(cell->getDelta(), delta);
    }
    return delta;
}
