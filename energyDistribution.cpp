#include <math.h>
#include "iostream"
#include "logger/logger.hpp"
#include "energyDistribution.hpp"
#include "base64/base64.hpp"

size_t EnergyDistribution::gCount = 0;
scaleType EnergyDistribution::interpolationType;
double EnergyDistribution::step = 0;
double EnergyDistribution::courantNumber = 0;
double* EnergyDistribution::eGroups = 0;
double* EnergyDistribution::width = 0;
double* EnergyDistribution::avgEinGroup = 0;

using namespace std;
using namespace Json;

EnergyDistribution::EnergyDistribution(){
	allocValues();
}
EnergyDistribution::EnergyDistribution(double value){
	allocValues();
	/* см. EnergyDistribution::operator = (double value) */
	*this = value;
}
EnergyDistribution::EnergyDistribution(const EnergyDistribution& src){
	allocValues();
	std::copy(src.values, src.values + gCount, this->values);
}
EnergyDistribution::~EnergyDistribution(){
	delete[] values;
}

void swap(EnergyDistribution& first, EnergyDistribution& second){
	std::swap(first.values, second.values);
}

EnergyDistribution EnergyDistribution::operator * (const EnergyDistribution& src){
	EnergyDistribution res;
	double* ptr = values;
	double* end = values + gCount;
	double* resptr = res.values;
	double* srcptr = src.values;
	while(ptr != end){
		*resptr++ = *ptr++ * *srcptr++;
	}
	return res;
}
EnergyDistribution EnergyDistribution::operator / (const EnergyDistribution& src){
	EnergyDistribution res;
	double* ptr = values;
	double* end = values + gCount;
	double* resptr = res.values;
	double* srcptr = src.values;
	while(ptr != end){
		*resptr++ = *ptr++ / *srcptr++;
	}
	return res;
}
EnergyDistribution EnergyDistribution::operator + (const EnergyDistribution& src){
	EnergyDistribution res;
	double* ptr = values;
	double* end = values + gCount;
	double* resptr = res.values;
	double* srcptr = src.values;
	while(ptr != end){
		*resptr++ = *ptr++ + *srcptr++;
	}
	return res;
}
EnergyDistribution EnergyDistribution::operator - (const EnergyDistribution& src){
	EnergyDistribution res;
	double* ptr = values;
	double* end = values + gCount;
	double* resptr = res.values;
	double* srcptr = src.values;
	while(ptr != end){
		*resptr++ = *ptr++ - *srcptr++;
	}
	return res;
}
EnergyDistribution EnergyDistribution::operator * (double mult){
	EnergyDistribution res;
	double* ptr = values;
	double* end = values + gCount;
	double* resptr = res.values;
	while(ptr != end){
		*resptr++ = *ptr++ * mult;
	}
	return res;
}
void EnergyDistribution::operator += (const EnergyDistribution& src){
	double* ptr = values;
	double* end = values + gCount;
	double* srcptr = src.values;
	while(ptr != end){
		*ptr++ += *srcptr++;
	}
}
void EnergyDistribution::operator -= (const EnergyDistribution& src){
	double* ptr = values;
	double* end = values + gCount;
	double* srcptr = src.values;
	while(ptr != end){
		*ptr++ -= *srcptr++;

	}
}

EnergyDistribution& EnergyDistribution::operator = (EnergyDistribution src){
	swap(*this, src);
	return *this;
}
EnergyDistribution& EnergyDistribution::equalize (EnergyDistribution& src){
	double* ptr = values;
	double* end = values + gCount;
	double* srcptr = src.values;
	while(ptr != end){
		*ptr++ = *srcptr++;
	}
	swap(*this, src);
	return *this;
}
EnergyDistribution& EnergyDistribution::operator = (double value){
	double* ptr = values;
	double* end = values + gCount;
	while(ptr != end){
		*ptr++ = value;
	}
	return *this;
}
void EnergyDistribution::operator *= (const EnergyDistribution& src){
	double* ptr = values;
	double* end = values + gCount;
	double* fptr = src.values;
	while(ptr != end){
		*ptr++ *= *fptr++;
	}
}
void EnergyDistribution::operator /= (const EnergyDistribution& src){
	double* ptr = values;
	double* end = values + gCount;
	double* fptr = src.values;
	while(ptr != end){
		*ptr++ /= *fptr++;
	}
}
void EnergyDistribution::operator *= (double mult){
	double* ptr = values;
	double* end = values + gCount;
	while(ptr != end){
		*ptr++ *= mult;
	}
}
void EnergyDistribution::operator /= (double mult){
	double* ptr = values;
	double* end = values + gCount;
	while(ptr != end){
		*ptr++ /= mult;
	}
}
/*
 * TODO: test
 */
bool EnergyDistribution::operator == (double value){
	double* ptr = values;
	double* end = values + gCount;
	while(ptr != end){
		if(*ptr++ != value){
			return false;
		}
	}
	return true;
}


EnergyDistribution EnergyDistribution::spreadDown(){
	EnergyDistribution res = 0;
	for(size_t i = 0; i < gCount; i++){
		double part = values[i] * width[i] /(eGroups[i]);
		for(size_t k = 0; k <= i; k++){
			res[k] += part;
		}
	}
	return res;
}
void EnergyDistribution::trimNegative(){
	double* ptr = values;
	double* end = values + gCount;
	while(ptr != end){
		if(*ptr < 0){
			*ptr = 0;
		}
		ptr++;
	}
}
EnergyDistribution EnergyDistribution::stretch(double mult){
	static double* erg = 0;
	if(!erg){
		erg = new double[2*gCount];
	}
	static double* val = 0;
	if(!val){
		val = new double[2*gCount];
	}
	if(mult == 0){
		return EnergyDistribution();
	}
	for(size_t i = 0; i < gCount; i++){
		val[2*i] = val[2*i + 1] = values[i] / mult;
		erg[2*i] = i == 0? 0:(eGroups[i - 1] * mult);
		erg[2*i + 1] = eGroups[i] * mult;
	}
	return EnergyDistribution::toEnergyGroups(erg, val, 2*gCount);
}
double EnergyDistribution::getInterpolatedByEnergy(double E){
	int index = -1;
	double ratio = 0;
	if(interpolationType == LINEAR){
		EnergyDistribution::getLinGroupAndRatioFor(E, index, ratio);
	}else{
		EnergyDistribution::getExpGroupAndRatioFor(E, index, ratio);
	}
	if(index < -1){
		LOG(ERROR) << "Bad index in getInterpolatedByEnergy: " << index;
		return 0;
	}else if(index == -1){
		return values[0] * ratio;
	}else if(index < (int)gCount - 1){
		return values[index] * ratio + values[index + 1] * (1 - ratio);
	}else if (index < (int)gCount){
		return values[index] * ratio;
	}else{
		return 0;
	}
}

void EnergyDistribution::getLinGroupAndRatioFor(double E, int& index, double& ratio){
	double _index = E/step - 0.5;
	double exactIndex = floor(_index);
	ratio = 1 - (_index - exactIndex);
	index = exactIndex;
}
/*
TODO: Написать эту функцию, когда разбиение на группы будет более очевидным
*/
void EnergyDistribution::getExpGroupAndRatioFor(double E, int& index, double& ratio){
	/*
	double _index = log(E/avgEinGroup[0])/step;
	double exactIndex = floor(_index);
	ratio = 1 - (_index - exactIndex);
	index = exactIndex;
	LOG(INFO) << "E: " << E <<", log(E): " << log(E) << ", index: " << _index << ", exact: " << exactIndex << ", ratio: " << ratio;
	*/
	LOG(ERROR) << "EnergyDistribution::getExpGroupAndRatioFor: Not implemented";
}
void EnergyDistribution::multAndAdd(const EnergyDistribution& src, double k){
	double* ptr = values;
	double* end = values + gCount;
	double* srcptr = src.values;
	while(ptr != end){
		*ptr++ += k * *srcptr++;
	}
}
void EnergyDistribution::multAndSub(const EnergyDistribution& src, double k){
	double* ptr = values;
	double* end = values + gCount;
	double* srcptr = src.values;
	while(ptr != end){
		*ptr++ -= k * *srcptr++;
	}
}
void EnergyDistribution::multAndAdd(const EnergyDistribution& src0, const EnergyDistribution& src1){
	double* ptr = values;
	double* end = values + gCount;
	double* src0ptr = src0.values;
	double* src1ptr = src1.values;
	while(ptr != end){
		*ptr++ += *src0ptr++ * *src1ptr++;
	}
}
void EnergyDistribution::multAndSub(const EnergyDistribution& src0, const EnergyDistribution& src1){
	double* ptr = values;
	double* end = values + gCount;
	double* src0ptr = src0.values;
	double* src1ptr = src1.values;
	while(ptr != end){
		*ptr++ -= *src0ptr++ * *src1ptr++;
	}
}


double& EnergyDistribution::operator[](size_t index){
	return values[index];
}
void EnergyDistribution::staticFlush(){
	delete[] eGroups;
	delete[] width;
	delete[] avgEinGroup;
	gCount = 0;
	courantNumber = 0;
}
void EnergyDistribution::setResolution(size_t count, double min, double max, scaleType type){
	staticFlush();
	gCount = count;
	double* data = EnergyDistribution::eGroups = new double[count];
	double* avg = EnergyDistribution::avgEinGroup = new double[count];
	interpolationType = type;
	EnergyDistribution::width = new double[count];
	if(type == LINEAR){
		step = (max - min) / (count);
		for(size_t i = 0; i < count; i++){
			data[i] = min + step * (i + 1);
		}
	}else{
		step = log(max / min) / (count - 1);
		for(size_t i = 0; i <count; i++){
			data[i] = min * exp(i * step);
		}
	}

	for(size_t i = 0; i < count; i++){
		avg[i] = (data[i] + (i?data[i-1] : 0))/2;
		width[i] = data[i] - (i?data[i-1] : 0);
	}
}

EnergyDistribution EnergyDistribution::toEnergyGroups(Json::Value& array){
	if(array.type() == Json::stringValue){
	    LOG(INFO) << "base64 encoded array in EnergyDistribution::toEnergyGroups";
	    vector<unsigned char> raw = base64::decode(array.asString());
	    double* data = (double*)&raw[0];
	    double* xOffset = data + 1;
	    double* yOffset = data + 2;
	    size_t stride = 2;
	    size_t count = (raw.size()/8 - 1)/2;
	    double leSign = data[0];
	    if(leSign != 1){
	        LOG(ERROR) << "Not a little endian";
	    }
	    double* erg = new double[count];
	    double* val = new double[count];
	    for(size_t i = 0; i < count; i++){
	        erg[i] = xOffset[stride*i];
	        val[i] = yOffset[stride*i];
	    }
	    EnergyDistribution res = toEnergyGroups(erg, val, count);

	    delete[] erg;
	    delete[] val;

	    return res;
	}

	if(array.type() == Json::stringValue){
	    LOG(ERROR) << "Not an array passed to EnergyDistribution::toEnergyGroups";
	}
    size_t count = array.size();
	if(count % 2 != 0){
		LOG(ERROR) << "Got wrong number of records while converting table to energy groups";
	}
	count /= 2;
	double* erg = new double[count];
	double* val = new double[count];

	for(Json::ArrayIndex i = 0; i < (Json::ArrayIndex)count; i++){
		erg[i] = array[2*i].asDouble();
		val[i] = array[2*i + 1].asDouble();
	}

	EnergyDistribution res = toEnergyGroups(erg, val, count);

	delete[] erg;
	delete[] val;

	return res;
}

EnergyDistribution EnergyDistribution::toEnergyGroups(double* erg, double* val, size_t count){
	if(gCount == 0){
		cerr << "No groups found while transforming reaction to table" <<endl;
	}
	EnergyDistribution res;
	size_t i = 0, j = 0, jr = 0;
	double prevX,
		prevY;

	while(erg[i] < 0 && i < count){
		i++;
	}
	if(i > 0){
		prevX = 0;
		prevY = interpolate(prevX, erg[i-1], erg[i], val[i-1], val[i]);
	}else{
		prevX = erg[0],
		prevY = val[0];
		i++;
	}

	while(prevX > eGroups[j] && gCount < j){
		j++;
	}

	for(; j < gCount; j++){
		double group = eGroups[j];
		double value = 0;
		while(i != count && erg[i] <= group){
			value += sum(prevX, erg[i], prevY, val[i]);
			prevX = erg[i];
			prevY = val[i];
			i++;
		}
		if(i != count && group > prevX){
			double itersectionY = interpolate(group, prevX, erg[i], prevY, val[i]);
			value += sum(prevX, group, prevY, itersectionY);
			prevX = group;
			prevY = itersectionY;
		}
		res[jr++] = value;
	}

	for(size_t j = 0; j < gCount; j++){
		double width = eGroups[j] - (j?eGroups[j-1] : 0);
		res[j] /= 2 * width;
	}

	return res;
}
Json::Value EnergyDistribution::toJson(){
	Json::Value res(objectValue);
	Json::Value groups(arrayValue);
	double total = 0;
	for(size_t i = 0; i < gCount; i++){
		total += values[i]*width[i];
		res[toStr(eGroups[i])] = values[i];
	}
	res["total"] = total;
	return res;
}
double EnergyDistribution::totalFlow(){
	double total = 0;
	for(size_t i = 0; i < gCount; i++){
		total += values[i];
	}
	return total;
}
bool EnergyDistribution::isNaN(){
	for(size_t i = 0; i < gCount; i++){
		if(isnan(values[i])){
			return true;
		}
	}
	return false;
}
std::ostream& operator << (std::ostream& to,EnergyDistribution& a){
	to << "top |\tavg |\tvalues\n";
	for(size_t i = 0; i < EnergyDistribution::gCount; i++){
		to << EnergyDistribution::eGroups[i] << " |\t" << EnergyDistribution::avgEinGroup[i]
		   << " |\t" << a[i] <<"\n";
	}
	return to;
}
std::string EnergyDistribution::getSpaceInfo(){
	std::stringstream ss;
	ss << "top |\tavg |\twidth\n";
	for(size_t i = 0; i < EnergyDistribution::gCount; i++){
		ss << EnergyDistribution::eGroups[i] << " |\t" << EnergyDistribution::avgEinGroup[i];
		ss << " |\t" << EnergyDistribution::width[i] <<"\n";
	}
	return ss.str();
}
void EnergyDistribution::setCourantNumber(double courant){
	EnergyDistribution::courantNumber = courant;
}
