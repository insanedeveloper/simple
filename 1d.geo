Point(1) = {0, 0, 0, 10.0};
Point(2) = {10, 0, 0, 10.0};
Point(3) = {10, 10, 0, 10.0};
Point(4) = {0, 10, 0, 10.0};
Line(1) = {1, 4};
Line(2) = {4, 3};
Line(3) = {3, 2};
Line(4) = {2, 1};
Line Loop(29) = {1, 2, 3, 4};
Plane Surface(30) = {29};
before[] = Extrude {0,0,5}{
    Surface{30}; Recombine; Layers{4};
};
middle[] = Extrude{0,0,25}{
    Surface{52}; Recombine; Layers{20};
};
after[] = Extrude{0,0,5}{
    Surface{74}; Recombine; Layers{4};
};
Physical Surface('source') = {30};
Physical Surface('mirror') = {43,47,51,39,61,65,69,73,83,87,91,95};
Physical Volume('void') = {1,3};
Physical Volume('shield') = {2};

