#ifndef ELREACT234345
#define ELREACT234345

#include "AbstractReaction.hpp"

class Elastic: public AbstractReaction {
public:
	Elastic();
	void apply(double mult, Momentum& src, Momentum& delta);
	int getReactionMT();
	void add(EnergyDistribution crossSection, double targetWeight);
	void add(EnergyDistribution crossSection);
	bool isNaN();
	void log();
private:
	friend class ElasticTest;
	vector<pair<double, EnergyDistribution> > crossSections;
	EnergyDistribution& getCrossSectionFor(double weight);

	const static int MT = 2;
	/* Получаем косинус в системе центра масс */
	static double getDotProductInCenterOfMass(double dot, double targetWeight);
	/* Распределение в системе центра масс */
	static double getAngularDistributionInCenterOfMass(double CoMdot);
	/* dcos(aCoM)/dcos(a) */
	static double getJacobian(double dot, double targetWeight);
	/* множитель энергии по углу рассеяния в СЦМ и массе мишени */
	static double getEnergyMultiplierByScatteringAngle(double CoMdot, double targetWeight);
};

#endif
