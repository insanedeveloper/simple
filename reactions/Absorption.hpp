#ifndef ABSREACT53453
#define ABSREACT53453

#include "SimpleReaction.hpp"

class Absorption: public SimpleReaction {
public:
	void apply(double mult, Momentum& src, Momentum& delta);
	int getReactionMT();
private:
	const static int MT = 27;
};

#endif
