#ifndef SIMPLEREACT53453
#define SIMPLEREACT53453

#include "AbstractReaction.hpp"

class SimpleReaction: public AbstractReaction {
public:
	SimpleReaction();
	void add(EnergyDistribution crossSection, double targetWeight);
	void add(EnergyDistribution crossSection);
	bool isNaN();
	void log();
protected:
	EnergyDistribution crossSection;
};

#endif
