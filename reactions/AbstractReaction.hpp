#ifndef BASEREACT23532
#define BASEREACT23532

#include "../momentum.hpp"


class AbstractReaction {
public:
	/**
	 * @param mult множитель, зависящий от внешних условий (плотность, время)
	 * @param src {Readonly} начальная ФР. Из неё нажно читать.
	 * @param delta {Writeonly} разница между конечной ФР и начальной, будет прибавлена к src после всех реакций.
	 * 		Вклад реакции нужно добавить к ней.
	 */
	virtual void apply(double mult, Momentum& src, Momentum& delta) = 0;
	virtual int getReactionMT() = 0;
	virtual void add(EnergyDistribution crossSection, double targetWeight) = 0;
	virtual bool isNaN() = 0;
	virtual void log() = 0;
protected:
	AbstractReaction();
	void substractReaction(EnergyDistribution& fraction, Momentum& src, Momentum& delta);
};
#endif
