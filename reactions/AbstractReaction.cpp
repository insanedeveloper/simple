#include "AbstractReaction.hpp"

AbstractReaction::AbstractReaction(){};

void AbstractReaction::substractReaction(EnergyDistribution& fraction, Momentum& src, Momentum& delta){
	size_t vCount = Momentum::getVectorCount();

	for(size_t i = 0; i < vCount; i++){
		EnergyDistribution& s = src.getGroupForVector(i);
		EnergyDistribution& d = delta.getGroupForVector(i);
		d.multAndSub(s, fraction);
	}
}
