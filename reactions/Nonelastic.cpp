#include "Nonelastic.hpp"

void Nonelastic::apply(double mult, Momentum& src, Momentum& delta){
	EnergyDistribution fraction = crossSection * mult;

	this->substractReaction(fraction, src, delta);

	fraction = src.getFlow() * fraction;
	fraction = fraction.spreadDown();

	delta += fraction;
}

int Nonelastic::getReactionMT(){
	return Nonelastic::MT;
}
