#include "Elastic.hpp"

Elastic::Elastic(){};

void Elastic::apply(double mult, Momentum& src, Momentum& delta){
    size_t count = crossSections.size();
    size_t vCount = Momentum::getVectorCount();

    for(size_t i = 0; i < count; i++){
        pair<double, EnergyDistribution>& component = crossSections[i];
        EnergyDistribution fraction = component.second * mult;

        double weight = component.first;
        double epsilon = (weight - 1)/(weight + 1);
        epsilon *= epsilon;
        for(size_t v1id = 0; v1id < vCount; v1id++){
            V& v1 = Momentum::getVector(v1id);
            EnergyDistribution s1 = src.getGroupForVector(v1id);
            EnergyDistribution& d1 = delta.getGroupForVector(v1id);
            double v1multiplier = Momentum::getdSForVector(v1id);
            EnergyDistribution income = s1;

            income *= fraction;

            /* Вычитание использованного из вектора 1 */
            d1 -= income;

            income *= v1multiplier;

            for(size_t v2id = v1id; v2id < vCount; v2id++){
                V& v2 = Momentum::getVector(v2id);
                double _dot = dot(v2, v1);
                EnergyDistribution& d2 = delta.getGroupForVector(v2id);

                /*
                 * Это условие было нужно, чтобы на 1 не было NaN'ов. Однако оно не дает получить
                 * сохранение потока при рассеянии, т.к. в dot == 0 всё-равно летят нейтроны в dS от него.
                 * if(dot > 0 || weight > 1){
                 */
                    double CoMdot = Elastic::getDotProductInCenterOfMass(_dot, weight);
                    /* FIXME: Нужно как минимум интегрировать распределение по dS, поэтому пока будем
                     * считать, что оно всегда изотропно в ЛС. Это должнобыть не очень плохое приближение.
                    double angularDistribution = Elastic::getAngularDistributionInCenterOfMass(CoMdot) * Elastic::getJacobian(dot, weight);
                    */
                    double angularDistribution = 1;
                    double eMultiplier = Elastic::getEnergyMultiplierByScatteringAngle(CoMdot, weight);

                    if(v2id != v1id){
                        EnergyDistribution s2 = src.getGroupForVector(v2id);

                        double v2multiplier = Momentum::getdSForVector(v2id);
                        EnergyDistribution outcome = s2;

                        outcome *= fraction;

                        outcome *= v2multiplier;

                        outcome = outcome.stretch(eMultiplier);
                        outcome *= angularDistribution;
                        d1 += outcome;
                    }

                    EnergyDistribution stretchedIncome = income.stretch(eMultiplier);
                    stretchedIncome *= angularDistribution;

                    d2 += stretchedIncome;

                //}
            }
        }
    }
}
bool Elastic::isNaN(){
    size_t count = crossSections.size();
    for(size_t i = 0; i < count; i++){
        if(crossSections[i].second.isNaN()){
            return true;
        }
    }
    return false;
}
void Elastic::log(){
    LOG(INFO) << "Reaction #" << this->getReactionMT() << ":";
    size_t count = crossSections.size();
    for(size_t i = 0; i < count; i++){
        LOG(INFO) << "Elem weight: " << crossSections[i].first << "\n" <<
                crossSections[i].second;
    }
}
void Elastic::add(EnergyDistribution crossSection, double targetWeight){
    EnergyDistribution& cs = this->getCrossSectionFor(targetWeight);
    crossSection.trimNegative();
    cs += crossSection;
}

EnergyDistribution& Elastic::getCrossSectionFor(double weight){
    size_t count = crossSections.size();
    for(size_t i = 0; i < count; i++){
        if(crossSections[i].first == weight){
            return crossSections[i].second;
        }
    }
    EnergyDistribution* p = new EnergyDistribution(0);
    crossSections.push_back(pair<double, EnergyDistribution>(weight, *p));
    return crossSections[crossSections.size() - 1].second;
}

int Elastic::getReactionMT(){
    return Elastic::MT;
}

double Elastic::getDotProductInCenterOfMass(double dot, double targetWeight){
    double w = dot*dot + targetWeight*targetWeight - 1;
    if(w < 0){
        return 0;
    }
    double res = (dot*sqrt(w) + dot*dot - 1)/targetWeight;
    if(res < -1.0){
        return -1;
    }
    if(res > 1.0){
        return 1;
    }
    return res;
}

/* TODO: придумать более вменяемый метод */
double Elastic::getAngularDistributionInCenterOfMass(double CoMdot){
    /* пока будет равномерное */
    return 1;
}

double Elastic::getJacobian(double dot, double targetWeight){
    if(targetWeight == 1){
        /* Для водорода в формуле получается деление на 0, хотя должен получаться осмысленный результат. */
        if(dot > 0){
            return 4 * dot;
        }else{
            return 0;
        }
    }
    double w = sqrt(dot*dot + targetWeight*targetWeight - 1);
    return (w + dot*dot/w + 2*dot)/targetWeight;
}

double Elastic::getEnergyMultiplierByScatteringAngle(double CoMdot, double targetWeight){
    double epsilon = (targetWeight - 1)/(targetWeight + 1);
    epsilon *= epsilon;
    return 0.5 * ((1 + epsilon) + (1 - epsilon)*CoMdot);
}
