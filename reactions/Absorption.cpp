#include "Absorption.hpp"

void Absorption::apply(double mult, Momentum& src, Momentum& delta){
	EnergyDistribution fraction = crossSection * mult;
	this->substractReaction(fraction, src, delta);
}

int Absorption::getReactionMT(){
	return Absorption::MT;
}
