#ifndef NONELREACT23423
#define NONELREACT23423

#include "SimpleReaction.hpp"

class Nonelastic: public SimpleReaction {
public:
	void apply(double mult, Momentum& src, Momentum& delta);
	int getReactionMT();
private:
	const static int MT = 3;
};

#endif
