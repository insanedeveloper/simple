#include "SimpleReaction.hpp"

SimpleReaction::SimpleReaction(){
	this->crossSection = 0;
};

void SimpleReaction::add(EnergyDistribution crossSection, double targetWeight){
	this->add(crossSection);
}
void SimpleReaction::add(EnergyDistribution crossSection){
	crossSection.trimNegative();
	this->crossSection += crossSection;
}
bool SimpleReaction::isNaN(){
	return crossSection.isNaN();
}
void SimpleReaction::log(){
	LOG(INFO) << "Reaction #" << this->getReactionMT() << '\n' << this->crossSection;
}
