#include <iostream>
#include <stdio.h>
#include <cstring>
#include <math.h>
#include <typeinfo>

#ifndef __BASETE4324__
#define __BASETE4324__

using namespace std;

class BaseTest {
protected:
	int status;/* !0 means failed */
	int counter;
	int failed;
	int passed;

	BaseTest(){
		status = counter = passed = failed = 0;
	}
	void fail(const char* message){
		status = 1;
		cerr << "Failed: " << message << endl;
	}
	void eq(int left, int right, const char* message){
		if(left == right){
			return;
		}
		char* msg = new char[strlen(message) + 100];
		sprintf(msg, "%s\n\tExpected: '%d', got: '%d'.", message, right, left);
		fail(msg);
		delete[] msg;
	}
	template <typename T, typename Y>
	void eq(T& left, Y& right, const char* message){
		if(left == right){
			return;
		}
		fail(message);
	}
	template <typename T, typename Y>
	void neq(T& left, Y right, const char* message){
		if(left != right){
			return;
		}
		fail(message);
	}
	void eq(double left, double right, const char* message){
		double delta = right? 1E-6 * right : 1E-6;
		if(fabs(left - right) < 1E-6 * delta ){
			return;
		}
		char* msg = new char[strlen(message) + 100];
		sprintf(msg, "%s\n\tExpected: '%le', got: '%le'.", message, right, left);
		fail(msg);
		delete[] msg;
	}
	void gt(double left, double right, const char* message){
		if(left > right ){
			return;
		}
		char* msg = new char[strlen(message) + 100];
		sprintf(msg, "%s\n\t'%le' greater than '%le'.", message, right, left);
		fail(msg);
		delete[] msg;
	}
	void clear(){
		status = 0;
		cout << "#### Running test #" << (++counter) << " ####" << endl;
	}
	int getStatusTextMessage(){
		if(status){
			failed++;
			cout<< "#### Test #" << counter << " failed ####" << endl;
			return status;
		}
		passed++;
		cout<< "#### Test #" << counter << " passed ####" << endl;
		return 0;
	}
	virtual int getTestSummary(){
		cout << typeid(*this).name() << ": " << passed << " passed, " << failed << " failed" << endl;
		return failed;
	}
};

#endif
