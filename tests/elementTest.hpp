#include "baseTest.hpp"
#include "../materials/element.hpp"
#include <stdlib.h>
#include <json/json.h>

#ifndef __ELEMENTEST2343__
#define __ELEMENTEST2343__

using namespace Json;

class ElementTest : BaseTest {
public:
	inline int run(){

		Value data = getTestJson();
		Element* elem = new Element(data);
		constructorTest(elem);
		return getTestSummary();
	}
private:
	int constructorTest(Element* elem){
		clear();
		eq(elem->atomicNumber, 2, "Wrong atomic number");
		eq(elem->massNumber, 4, "Wrong mass number");
		eq(elem->temperature, 300.0, "Wrong mass temperature");
		eq(elem->crossSections.size(), 1, "Wrong crossSections count");
		map<int, EnergyDistribution>::iterator crossSection = elem->crossSections.find(1);
		if(crossSection == elem->crossSections.end()){
			fail("Wrong crossSection number");
		}
		return getStatusTextMessage();
	}
	Json::Value getTestJson(){
		Value data(objectValue);
		data["atomicNumber"] = Value(2);
		data["massNumber"] = Value(4);
		data["temperature"] = Value(300.0);
		Value reactions(objectValue);
		Value reaction(arrayValue);

		for(int i = 0; i < 10; i++){
			reaction[2*i] = i + 0.2;
			reaction[2*i+1] = Value(2 + 0.5 * i);
		}
		reactions["1"] = reaction;
		data["reactions"] = reactions;

		return data;
	}
};
#endif
