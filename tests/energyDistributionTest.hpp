#include "baseTest.hpp"
#include "../momentum.hpp"

#ifndef __ENERGYDISTRTEST435__
#define __ENERGYDISTRTEST435__

#include <json/json.h>

class EnergyDistributionTest : BaseTest {
public:
	inline int run(){
		setResolutionTest() ||
		constructorTest() ||
		operatorTest() ||
		multAndWhateverTest() ||
		toEnergyGroupsTest() ||
		toEnergyGroupsTest2() ||
		getLinGroupAndRatioForTest() ||
		// getExpGroupAndRatioForTest() ||
		getInterpolatedByEnergyTest() ||
		spreadDownTest() ||
		stretchTest();
		return getTestSummary();
	}
private:
	int constructorTest(){
		clear();
		EnergyDistribution t;
		t = 0;
		size_t size = EnergyDistribution::gCount;
		for(size_t i = 0; i < size; i++){
			eq(t.values[i], .0, "values");
		}
		return getStatusTextMessage();
	}
	int setResolutionTest(){
		clear();
		{
			EnergyDistribution::setResolution(2, 0, 10, LINEAR);
			double right[] = { 5, 10 };
			for(size_t i = 0; i < 2; i++){
				eq(EnergyDistribution::eGroups[i], right[i], "wrong value at 1");
			}
		}
		{
			EnergyDistribution::setResolution(2, 1, 10, LOGARITHMIC);
			double right[] = { 1, 10 };
			for(size_t i = 0; i < 2; i++){
				eq(EnergyDistribution::eGroups[i], right[i], "wrong value at 2");
			}
		}
		{
			EnergyDistribution::setResolution(4, 2, 10, LINEAR);
			double right[] = { 4, 6, 8, 10 };
			for(size_t i = 0; i < 4; i++){
				eq(EnergyDistribution::eGroups[i], right[i], "wrong value at 3");
			}
		}
		{
			EnergyDistribution::setResolution(5, 1, 10000, LOGARITHMIC);
			double right[] = { 1, 10, 100, 1000, 10000 };
			for(size_t i = 0; i < 5; i++){
				eq(EnergyDistribution::eGroups[i], right[i], "wrong value at 4");
			}
		}
		return getStatusTextMessage();
	}

	int operatorTest(){
		clear();
		EnergyDistribution::setResolution(10, 1, 10, LINEAR);
		EnergyDistribution a, b;
		b = 0;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			a[j] = EnergyDistribution::eGroups[j];
			eq(b[j], .0, "Wrong result at operator= 1");
		}
		b += a;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], a[j], "Wrong result at operator+ 1");
		}
		b += b;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], 2 * a[j], "Wrong result at operator+ 2");
		}
		b = a;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], a[j], "Wrong result at operator= 2");
		}
		EnergyDistribution c = a * b;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(c[j], (a[j]) * (b[j]), "Wrong result at operator *");
		}
		a *= b;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(a[j], (b[j]) * (b[j]), "Wrong result at operator*=");
		}


		return getStatusTextMessage();
	}

	int multAndWhateverTest(){
		clear();
		EnergyDistribution::setResolution(10, 1, 10, LINEAR);
		EnergyDistribution a, b = 0;
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			a[j] = EnergyDistribution::eGroups[j];
		}
		b.multAndAdd(a, 0.5);
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], 0.5*a[j], "Wrong result at multAndAdd 1");
		}
		b.multAndAdd(a, 0.5);
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], a[j], "Wrong result at multAndAdd 2");
		}
		b.multAndSub(a, 0.5);
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], 0.5*a[j], "Wrong result at multAndSub 1");
		}
		b.multAndSub(a, 0.5);
		for(size_t j = 0; j < EnergyDistribution::gCount; j++){
			eq(b[j], .0, "Wrong result at multAndSub 2");
		}
		return getStatusTextMessage();
	}

	int toEnergyGroupsTest(){
		clear();
		EnergyDistribution::setResolution(4, 0, 4, LINEAR);
		{
			double erg[] = {0.5, 1.5, 3.5};
			double val[] = {1, 1, 1};
			double ans[] = {0.5, 1, 1, 0.5};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 3);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at 0");
			}
		}
		{
			double erg[] = {0.5, 1.5, 3.5};
			double val[] = {2, 2, 0};
			double ans[] = {1, 1.875, 1, 0.125};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 3);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at 1");
			}
		}
		{
			double erg[] = {-1, 1.5, 3.5};
			double val[] = {0, 2.5, 0.5};
			double ans[] = {1.5, 2.25, 1.5, 0.375};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 3);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at left end");
			}
		}
		{
			double erg[] = {0.5, 2.5, 5};
			double val[] = {2.5, 2.5, 0};
			double ans[] = {1.25, 2.5, 2.375, 1.5};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 3);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at right end");
			}
		}
		{
			double erg[] = {0, 1, 2, 3, 4};
			double val[] = {1, 2, 4, 1, 2};
			double ans[] = {1.5, 3, 2.5, 1.5};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 5);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at exact limits");
			}
		}
		{
			double erg[] = {0, 1, 1, 2, 2, 3, 3, 4};
			double val[] = {1, 1, 2, 2, 4, 4, 1, 1};
			double ans[] = {1, 2, 4, 1};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 8);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at histogram");
			}
		}
		{
			double erg[] = {0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 3.25, 3.5, 3.75, 4.25};
			double val[] = {   0,   4,    2, 2,    3,   4,    1, 0,    1,   4,    2,    1};
			double ans[] = {1.75, 2.25, 0.4, 2.0375};
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 12);
			for(size_t i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at lots of points");
			}
		}
		return getStatusTextMessage();
	}

	int toEnergyGroupsTest2(){
	    clear();
	    EnergyDistribution::setResolution(4, 0, 4, LINEAR);
	    {
            Json::Value array(Json::arrayValue);

            for(int i = 0; i < 4; i++){
                array[2*i] = Json::Value(i);
                array[2*i+1] = Json::Value(2*i);
            }
            EnergyDistribution test = EnergyDistribution::toEnergyGroups(array);
            double ans[] = {1, 3, 5, 0};
            for(size_t i = 0; i<4; i++){
                eq(test[i], ans[i], "Wrong values at arrayValue");
            }
        }
        {
            /*
             * base64 значение от буфера из 9 double'ов в LE:
             * первый из них контрольный, равен 1; остальные как из теста выше
             */
            Json::Value array = "AAAAAAAA8D8AAAAAAAAAAAAAAAAAAAAAAAAAAAAA8D8AAAAAAAAAQAAAAAAAAABAAAAAAAAAEEAAAAAAAAAIQAAAAAAAABhA";
            EnergyDistribution test = EnergyDistribution::toEnergyGroups(array);
            double ans[] = {1, 3, 5, 0};
            for(size_t i = 0; i<4; i++){
                eq(test[i], ans[i], "Wrong values at stringValue");
            }
        }

        return getStatusTextMessage();
    }

	int getLinGroupAndRatioForTest(){
		clear();
		EnergyDistribution::setResolution(10, 0, 100, LINEAR);
		cout << EnergyDistribution::getSpaceInfo();
		int index;
		double k;
		{
			EnergyDistribution::getLinGroupAndRatioFor(0, index, k);
			eq(index, -1, "Wrong index at 0");
			eq(k, 0.5, "Wrong ratio at 0");
		}
		{
			EnergyDistribution::getLinGroupAndRatioFor(10, index, k);
			eq(index, 0, "Wrong index at 1");
			eq(k, 0.5, "Wrong ratio at 1");
		}
		{
			EnergyDistribution::getLinGroupAndRatioFor(5, index, k);
			eq(index, 0, "Wrong index at 2");
			eq(k, 1., "Wrong ratio at 2");
		}
		{
			EnergyDistribution::getLinGroupAndRatioFor(99, index, k);
			eq(index, 9, "Wrong index at 3");
			eq(k, 0.6, "Wrong ratio at 3");
		}
		{
			EnergyDistribution::getLinGroupAndRatioFor(100, index, k);
			eq(index, 9, "Wrong index at 4");
			eq(k, 0.5, "Wrong ratio at 4");
		}
		{
			EnergyDistribution::getLinGroupAndRatioFor(105, index, k);
			eq(index, 10, "Wrong index at 5");
			eq(k, 1., "Wrong ratio at 5");
		}

		return getStatusTextMessage();
	}
	/* TODO: исправить и включить этот тест после исправления функции */
	int getExpGroupAndRatioForTest(){
		clear();
		EnergyDistribution::setResolution(10, 1E-4, 1E5, LOGARITHMIC);
		cout << EnergyDistribution::getSpaceInfo();
		int index;
		double k;
		{
			EnergyDistribution::getExpGroupAndRatioFor(5E-5, index, k);
			eq(index, 0, "Wrong index at 0");
			eq(k, 1., "Wrong ratio at 0");
		}
		{
			EnergyDistribution::getExpGroupAndRatioFor(55, index, k);
			eq(index, 5, "Wrong index at 1");
			eq(k, 1., "Wrong ratio at 1");
		}
		{
			EnergyDistribution::getExpGroupAndRatioFor(0.4, index, k);
			eq(index, 0, "Wrong index at 2");
			eq(k, 0.6, "Wrong ratio at 2");
		}
		{
			EnergyDistribution::getExpGroupAndRatioFor(9.9, index, k);
			eq(index, 9, "Wrong index at 3");
			eq(k, 0.1, "Wrong ratio at 3");
		}
		{
			EnergyDistribution::getExpGroupAndRatioFor(10, index, k);
			eq(index, 10, "Wrong index at 4");
			eq(k, 1., "Wrong ratio at 4");
		}
		{
			EnergyDistribution::getExpGroupAndRatioFor(10.5, index, k);
			eq(index, 10, "Wrong index at 5");
			eq(k, 0.5, "Wrong ratio at 5");
		}

		return getStatusTextMessage();
	}
	int getInterpolatedByEnergyTest(){
		clear();

		EnergyDistribution::setResolution(10, 0, 100, LINEAR);
		cout << EnergyDistribution::getSpaceInfo();

		double erg[] = {0, 10, 20, 30, 100};
		double val[] = {0,  1,  4,  0,   1};
		EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 5);
		cout << test;

		eq(test.getInterpolatedByEnergy(0), 0.25,  "Wrong E at 0");
		eq(test.getInterpolatedByEnergy(5), 0.5,   "Wrong E at 1");
		eq(test.getInterpolatedByEnergy(10), 1.5,  "Wrong E at 2");
		eq(test.getInterpolatedByEnergy(11), 1.7,  "Wrong E at 3");
		eq(test.getInterpolatedByEnergy(15), 2.5,  "Wrong E at 4");
		eq(test.getInterpolatedByEnergy(20), 2.25, "Wrong E at 5");
		eq(test.getInterpolatedByEnergy(25), 2.,   "Wrong E at 6");
		eq(test.getInterpolatedByEnergy(29), 2*0.6 + 0.4*.5/7,    "Wrong E at 7");
		eq(test.getInterpolatedByEnergy(80), 5./7, "Wrong E at 8");
		eq(test.getInterpolatedByEnergy(105), 0.,  "Wrong E at 9");
		eq(test.getInterpolatedByEnergy(100), (1-0.5/7)/2, "Wrong E at 10");
		eq(test.getInterpolatedByEnergy(-10), .0,  "Wrong E at 11");

		return getStatusTextMessage();
	}

	int spreadDownTest(){
		clear();
		double erg[] = {0, 10};
		double val[] = {1, 1};
		{
			EnergyDistribution::setResolution(4, 0, 4, LINEAR);
			EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 2);
			LOG(INFO) << "\n" << test;
			double totFlow = test.totalFlow();
			test = test.spreadDown();
			double ans[] = {
				1 + 0.5 + (1./3) + 0.25,
					0.5 + (1./3) + 0.25,
						  (1./3) + 0.25,
					    	 	   0.25
			};
			for(int i = 0; i<4; i++){
				eq(test[i], ans[i], "Wrong values at 4x4");
			}
			eq(test.totalFlow() - totFlow, 0., "Leakage");
		}
		return getStatusTextMessage();
	}

	int stretchTest(){
		clear();
		double erg[] = {0, 5};
		double val[] = {1, 1};

		{
					EnergyDistribution::setResolution(10, 0, 10, LINEAR);
					EnergyDistribution test = EnergyDistribution::toEnergyGroups(erg, val, 2);
					LOG(INFO) << "\n" << test << endl << test.totalFlow();;
					double totFlow = test.totalFlow();
					test = test.stretch(0.99);
					LOG(INFO) << "\n" << test << endl << test.totalFlow();
					eq(test.totalFlow() - totFlow, 0., " stretch test Leakage");
		}
		return getStatusTextMessage();
	}
};
#endif
