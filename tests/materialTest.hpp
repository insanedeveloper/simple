#include "baseTest.hpp"
#include "../materials/material.hpp"
#include <stdlib.h>
#include <json/json.h>

#ifndef __MATERIALTEST34534__
#define __MATERIALTEST34534__

using namespace Json;

class MaterialTest : BaseTest {
public:
	inline int run(){
		constructorTest();
		getReactionTest();
		return getTestSummary();
	}
private:
	int constructorTest(){
		clear();
		Material::fillHRN();
		{
            Material* m = new Material("atomicFractions", this->getFakeMaterialAtomic());
            eq(m->cleanList[0].second, 0.75, "Li6");
            eq(m->cleanList[1].second, 0.25, "O");
            delete m;
		}
		{
            Material* m = new Material("massFractions", this->getFakeMaterialMass());
            eq(m->cleanList[0].second, 0.8, "Li6");
            eq(m->cleanList[1].second, 0.2, "O");
            delete m;
		}
		return getStatusTextMessage();
	}
	int getReactionTest(){
		clear();
		Material* m = new Material("fake", this->getFakeMaterial());
		eq(m->reactions.size(), 0, "No reactions");
		AbstractReaction* r = m->getReaction(REACTION_EL);
		eq(m->reactions.size(), 1, "New reaction added");
		eq(r->getReactionMT(), REACTION_EL, "Correct constructor was invoked");
		return getStatusTextMessage();
	}
	Json::Value getFakeMaterial(){
		Value data(arrayValue);
		Value elem0(objectValue);
		elem0["name"] = Value("Li");
		elem0["weight"] = Value(6);
		elem0["massFraction"] = Value(6.0/13);
		Value elem1(objectValue);
		elem1["name"] = Value("Li");
		elem1["weight"] = Value(7);
		elem1["atomicFraction"] = Value(0.5);
		data[0] = elem0;
		data[1] = elem1;
		return data;
	}
    Json::Value getFakeMaterialAtomic(){
        Value data(arrayValue);
        Value elem0(objectValue);
        elem0["name"] = Value("Li");
        elem0["weight"] = Value(6);
        elem0["atomicFraction"] = Value(4);
        Value elem1(objectValue);
        elem1["name"] = Value("O");
        elem1["weight"] = Value(16);
        elem1["atomicFraction"] = Value(0.5);
        data[0] = elem0;
        data[1] = elem1;
        return data;
    }
    Json::Value getFakeMaterialMass(){
        Value data(arrayValue);
        Value elem0(objectValue);
        elem0["name"] = Value("Li");
        elem0["weight"] = Value(6);
        elem0["massFraction"] = Value(8.0);
        Value elem1(objectValue);
        elem1["name"] = Value("O");
        elem1["weight"] = Value(16);
        elem1["massFraction"] = Value(2.0);
        data[0] = elem0;
        data[1] = elem1;
        return data;
    }
};
#endif
