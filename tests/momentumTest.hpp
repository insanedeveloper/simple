#include "baseTest.hpp"
#include "../momentum.hpp"

#ifndef __MOMENTTEST234__
#define __MOMENTTEST234__

class MomentumTest : BaseTest {
public:
	inline int run(){
		capAreaTest() ||
				capAngleTest() ||
				capAreaAndAngleTest(20) ||
				resizeSpaceToTest(2000) ||
				findClosestForTest() ||
				findTripleForTest();
		return getTestSummary();
	}
private:
	int capAreaTest(){
		clear();
		eq(Momentum::capArea(0), .0, "CapArea returned wrong value at 0");
		eq(Momentum::capArea(M_PI), 4*M_PI, "CapArea returned wrong value at Pi");
		eq(Momentum::capArea(M_PI_2), 2*M_PI, "CapArea returned wrong value at Pi/2");

		return getStatusTextMessage();
	}
	int capAngleTest(){
		clear();
		eq(Momentum::capAngle(0), .0, "CapAngle returned wrong value at 0");
		eq(Momentum::capAngle(4*M_PI), M_PI, "CapAngle returned wrong value at 4Pi");
		eq(Momentum::capAngle(2*M_PI), M_PI_2, "CapAngle returned wrong value at 2Pi");

		return getStatusTextMessage();
	}
	int capAreaAndAngleTest(int segments){
		clear();
		double angle, area;
		double segA = M_PI / segments;
		for(int i = 0; i < segments; i++){
			angle = i * segA;
			area = 4 * i * segA;
			eq(Momentum::capAngle(Momentum::capArea(angle)), angle, "angle->area");
			eq(Momentum::capArea(Momentum::capAngle(area)), area, "area->angle");
		}
		return getStatusTextMessage();
	}
	int resizeSpaceToTest(int numVectors){
		clear();
		Momentum::resizeSpaceTo(numVectors);

		V sum(0, 0, 0);
		for(int i = 0; i < numVectors; i++){
			sum += Momentum::dirVectors[i];
			/*
			cout<<endl<<Momentum::dirAngles[i].first  * 360 / (2 * M_PI) << " "<< Momentum::dirAngles[i].second  * 360 / (2 * M_PI)<<endl;
			*/
		}

		eq(sqr(sum), .0, "Total momentum is not null");
		return getStatusTextMessage();
	}
	int findClosestForTest(){
		clear();
		{
			V dir(0, 0, 1);
			dir = normalize(dir);
			Momentum::resizeSpaceTo(6);
			eq(Momentum::findClosestFor(dir), 0, "Wrong point index at 1");
		}
		{
			V dir(1, 1, 0);
			dir = normalize(dir);
			Momentum::resizeSpaceTo(6);
			eq(Momentum::findClosestFor(dir), 1, "Wrong point index at 2");
		}
		return getStatusTextMessage();
	}
	int findTripleForTest(){
		clear();
		int indexes[3];
		double fractions[3];
		{
			V dir(sqrt(2), 0, 1);
			Momentum::resizeSpaceTo(6);
			dir = normalize(dir);
			Momentum::findTripleFor(dir, indexes, fractions);
			int correct[] = {4, 0, 1};
			for(int i = 0; i < 3; i++){
				eq(indexes[i], correct[i], "Wrong point index at 1");
				eq(fractions[i],1/sqrt(3), "Wrong fraction at 1");
			}
		}

		return getStatusTextMessage();
	}
};

#endif
