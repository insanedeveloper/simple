#include "baseTest.hpp"
#include "../reactions/Elastic.hpp"
#include <stdlib.h>
#include <json/json.h>

#ifndef __ELASTICTEST353445__
#define __ELASTICTEST353445__

using namespace Json;

class ElasticTest : BaseTest {
public:
	inline int run(){
		getDotProductInCenterOfMassTest();
		getJacobianTest();
		getEnergyMultiplierByScatteringAngleTest();
		flowEqualityTest();
		return getTestSummary();
	}
private:
	int getDotProductInCenterOfMassTest(){
		clear();
		eq(Elastic::getDotProductInCenterOfMass(1, 1), 1.0, "1 1");
		eq(Elastic::getDotProductInCenterOfMass(-1, 1) + 1, .0, "-1 1");
		eq(Elastic::getDotProductInCenterOfMass(0, 1) + 1, .0, "0 1");
		eq(Elastic::getDotProductInCenterOfMass(1, 1E23), 1.0, "1 1E23");
		eq(Elastic::getDotProductInCenterOfMass(-1, 1E23) + 1, .0, "-1 1E23");
		eq(Elastic::getDotProductInCenterOfMass(0, 1E23), .0, "0 1E23");
		return getStatusTextMessage();
	}
	int getJacobianTest(){
		clear();
		eq(Elastic::getJacobian(1, 1), 4.0, "1 1");
		eq(Elastic::getJacobian(-1, 1) , .0, "-1 1");
		eq(Elastic::getJacobian(0, 1), .0, "0 1");

		eq(Elastic::getJacobian(1, 2), 2.25, "1 2");
		eq(Elastic::getJacobian(-1, 2) , 0.25, "-1 2");
		eq(Elastic::getJacobian(0, 2), sqrt(3)/2, "0 2");

		eq(Elastic::getJacobian(1, 1E23), 1.0, "1 1E23");
		eq(Elastic::getJacobian(-1, 1E23), 1.0, "-1 1E23");
		eq(Elastic::getJacobian(0, 1E23), 1.0, "0 1E23");
		return getStatusTextMessage();
	}
	int getEnergyMultiplierByScatteringAngleTest(){
		clear();

		return getStatusTextMessage();
	}

	int flowEqualityTest(){
		clear();
		EnergyDistribution::setResolution(10, 0, 1, LINEAR);
		size_t VECTOR_COUNT = 32;
		Momentum::resizeSpaceTo(VECTOR_COUNT);
		Momentum space, buffer;
		EnergyDistribution tmp;


		{
			/* infinite mass */
			Elastic r;
			r.add(EnergyDistribution(1), 1E20);
			space.clear();
			buffer.clear();
			EnergyDistribution& erg = space.getGroupForVector(0);
			erg = 1;
			tmp = space.getFlow();
			LOG(INFO) << tmp;
			r.apply(1, space, buffer);

			tmp = buffer.getFlow();
			LOG(INFO) << tmp;

			space += buffer;
			tmp = space.getFlow();
			LOG(INFO) << tmp;
			LOG(INFO) << erg;
			eq(buffer.getFlow().totalFlow(), .0, "Scatter on infinite mass");
		}
		{
			/* real H */
			LOG(INFO) << "Scatter on real? H";
			Elastic r;
			r.add(EnergyDistribution(1), 0.99917);
			space.clear();
			buffer.clear();
			EnergyDistribution& erg = space.getGroupForVector(0);
			erg = VECTOR_COUNT/(4*M_PI);
			tmp = space.getFlow();
			LOG(INFO) << "init flow" << endl << tmp;
			LOG(INFO) << "init distribution" << endl << erg;
			r.apply(1, space, buffer);

			tmp = buffer.getFlow();
			LOG(INFO) << "flow delta in buffer" << endl << tmp;

			space += buffer;
			tmp = space.getFlow();
			LOG(INFO) << "end flow" << endl << tmp;
			LOG(INFO) << "end distribution" << endl << erg;
			eq(buffer.getFlow().totalFlow(), .0, "Scatter on real? H");
		}
		{
			/* real H #2 */
			LOG(INFO) << "Scatter on real?? H";
			Elastic r;
			r.add(EnergyDistribution(1), 1.007825);
			space.clear();
			buffer.clear();
			EnergyDistribution& erg = space.getGroupForVector(0);
			erg = VECTOR_COUNT/(4*M_PI);
			tmp = space.getFlow();
			LOG(INFO) << "init flow" << endl << tmp;
			LOG(INFO) << "init distribution" << endl << erg;
			r.apply(1, space, buffer);

			tmp = buffer.getFlow();
			LOG(INFO) << "flow delta in buffer" << endl << tmp;

			space += buffer;
			tmp = space.getFlow();
			LOG(INFO) << "end flow" << endl << tmp;
			LOG(INFO) << "end distribution" << endl << erg;
			eq(buffer.getFlow().totalFlow(), .0, "Scatter on real?? H");
		}
		{
			/* D */
			LOG(INFO) << "Scatter on D";
			Elastic r;
			r.add(EnergyDistribution(1), 2);
			space.clear();
			buffer.clear();
			EnergyDistribution& erg = space.getGroupForVector(0);
			erg = VECTOR_COUNT/(4*M_PI);
			tmp = space.getFlow();
			LOG(INFO) << "init flow" << endl << tmp;
			LOG(INFO) << "init distribution" << endl << erg;
			r.apply(1, space, buffer);

			tmp = buffer.getFlow();
			LOG(INFO) << "flow delta in buffer" << endl << tmp;

			space += buffer;
			tmp = space.getFlow();
			LOG(INFO) << "end flow" << endl << tmp;
			LOG(INFO) << "end distribution" << endl << erg;
			eq(buffer.getFlow().totalFlow(), .0, "Scatter on D");
		}
		{
			/* D */
			LOG(INFO) << "Scatter on 14";
			Elastic r;
			r.add(EnergyDistribution(1), 14);
			space.clear();
			buffer.clear();
			EnergyDistribution& erg = space.getGroupForVector(0);
			erg = VECTOR_COUNT/(4*M_PI);
			tmp = space.getFlow();
			LOG(INFO) << "init flow" << endl << tmp;
			LOG(INFO) << "init distribution" << endl << erg;
			r.apply(1, space, buffer);

			tmp = buffer.getFlow();
			LOG(INFO) << "flow delta in buffer" << endl << tmp;

			space += buffer;
			tmp = space.getFlow();
			LOG(INFO) << "end flow" << endl << tmp;
			LOG(INFO) << "end distribution" << endl << erg;
			eq(buffer.getFlow().totalFlow(), .0, "Scatter on 14");
		}
		return getStatusTextMessage();
	}
};
#endif
