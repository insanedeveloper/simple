#include "baseTest.hpp"
#include "../source.hpp"
#include <json/json.h>

#ifndef __SOURCETEST234__
#define __SOURCETEST234__

using namespace Json;

class SourceTest : BaseTest {
public:
	inline int run(){
		prepareEnvironment();
		constructorTest();
		return getTestSummary();
	}
private:
	void prepareEnvironment(){
		EnergyDistribution::setResolution(30, 1, 3, LINEAR);
	}
	Json::Value getTestJson(double x, double y, double z){
		Value data(objectValue);

		Value dir(arrayValue);
		dir[0u] = x;
		dir[1] = y;
		dir[2] = z;
		data["direction"] = dir;

		Value spectrum(arrayValue);
		spectrum[0u] = 1;
		spectrum[1] = 1;
		spectrum[2] = 3;
		spectrum[3] = 1;
		data["spectrum"] = spectrum;

		return data;
	}
	int constructorTest(){
		clear();
		Momentum::resizeSpaceTo(8);
		Value json = getTestJson(1, 1, 0);
		Source* s = new Source("test", json);
		Momentum& distr = s->distr;

		neq(distr, 0, "source distribution == 0");

		return getStatusTextMessage();
	}
};
#endif
