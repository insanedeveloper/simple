#include <iostream>
#include "energyDistributionTest.hpp"
#include "momentumTest.hpp"
#include "sourceTest.hpp"
#include "elementTest.hpp"
#include "materialTest.hpp"
#include "elasticTest.hpp"

int main(int argc, char* argv[]){

	EnergyDistributionTest test0;
	MomentumTest test1;
	SourceTest test2;
	ElementTest test3;
	MaterialTest test4;
	ElasticTest test5;
	return test0.run() || test1.run() || test2.run() || test3.run() || test4.run() || test5.run();

	return 0;
}
