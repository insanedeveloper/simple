#include "Throttle.hpp"
#include <ctime>
#include <sys/time.h>
#include <stdarg.h>

Throttle::Throttle(void(*func)(double a, double b, double c, int d, std::string e), long interval){
    this->func = func;
    this->interval = interval;
    this->lastcall = 0;
}
void Throttle::run(double a, double b, double c, int d, std::string e){
    timeval tv;
    gettimeofday(&tv, NULL);

    long now = tv.tv_sec;
    if(now - lastcall > interval){

        lastcall = now;
        (this->func)(a, b, c, d, e);
    }
}
