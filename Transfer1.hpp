#ifndef TRANSFER1_HPP_
#define TRANSFER1_HPP_

#include "Transfer.hpp"

class Transfer1 : public Transfer {
    public:
        void move(Mesh& mesh);
        Transfer1(Stat& stat){
        	this->stat = &stat;
        }
    protected:
        void markAsMelted(Mesh& mesh);
};

#endif /*TRANSFER1_HPP_*/
