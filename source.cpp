#include "source.hpp"

using namespace Json;

map<std::string, Source*> Source::srcList;

Source::~Source(){
	map<std::string, Source*>::iterator ptr = Source::srcList.find(id);
	if(ptr != Source::srcList.end()){
		Source::srcList.erase(ptr);
	}
}
Source::Source(std::string name, Json::Value& src){
	this->id = name;
	Value& direction = src["direction"];
	V dir(direction[0].asDouble(), direction[1].asDouble(), direction[2].asDouble());
	dir = normalize(dir);
	cout<< dir << "a"<<endl;
	int indexes[3];
	/*double fractions[3];*/
	indexes[0] = Momentum::findClosestFor(dir);

	EnergyDistribution spectrum;

	if(src.isMember("file")){
		Value spJson = loadSpectrumFromFile(src["file"].asString());
		spectrum = EnergyDistribution::toEnergyGroups(spJson);
	}else{
		spectrum = EnergyDistribution::toEnergyGroups(src["spectrum"]);
	}

	for(int i = 0; i < 1; i++){
		EnergyDistribution& values = distr.getGroupForVector(indexes[i]);
		values.multAndAdd(spectrum, 1/Momentum::getdSForVector(indexes[i]));
	}
	Source::srcList[name] = this;
}

void Source::load(Json::Value& data){
	Value::Members names = data.getMemberNames();
	size_t count = names.size();
	for(size_t i = 0; i < count; i++){
		new Source(names[i], data[names[i]]);
	}
}

Source* Source::getByName(std::string name){
	return Source::srcList[name];
}

Json::Value Source::loadSpectrumFromFile(std::string filename){
	Json::Value json;
	Json::Reader reader;
	bool parsingSuccessful = reader.parse( filename, json );
	if ( !parsingSuccessful ){
	    // report to the user the failure and their locations in the document.
		 LOG(ERROR)  << "Failed to parse file: " << filename << endl
	               << reader.getFormatedErrorMessages();
	    return Value();
	}
	return json;
}
