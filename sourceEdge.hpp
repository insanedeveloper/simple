#include "edge.hpp"

#ifndef __SRCEDG324__
#define __SRCEDG324__

class SourceEdge : public Edge{
public:
	SourceEdge(Cell* cell, Momentum& distr, V n){
		left = cell;
		src = distr;
		this->n = n;
	}
	SourceEdge(Cell* cell, double energy, double intensity, V direction, V n){
		left = cell;
		src.addAt(direction, energy, intensity, CLOSEST);
		this->n = n;
	}
	void transfer();
private:
	Momentum src;
};
#endif
