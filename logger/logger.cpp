#include <iomanip>
#include <ctime>
#include <sys/time.h>

#include "logger.hpp"

namespace logger {

const char* severity_names[] = {"[I]",
                                "[W]",
                                "[E]"};

std::map<std::string, long> Message::delays;

std::ostringstream& Message::stream(Severity l, std::string resource, long interval){
	timeval tv;
	gettimeofday(&tv, NULL);

	long now = tv.tv_sec;
	if(now - delays[resource] > interval){
		delays[resource] = now;
		stream(l);
		os  << resource << ": ";
		return os;
	}else{
		return nul;
	}
}

std::ostringstream& Message::stream(Severity l)
{
    level = l;

    timeval tv;
    gettimeofday(&tv, NULL);

    tm* tm_p;
    tm_p = localtime(&tv.tv_sec);

    using namespace std;

    os << severity_names[static_cast<int>(level)] << ' ' 
       << setfill('0') 
       << setw(2) << tm_p->tm_hour << ':' 
       << setw(2) << tm_p->tm_min  << ':' 
       << setw(2) << tm_p->tm_sec  << '.' << tv.tv_usec 
       << " : ";

    return os;
}

Message::~Message()
{
	std::string msg = os.str();
	if(msg.length() != 0){
		std::cerr << msg << std::endl;
	}
}

} // namespace logger

