#ifndef _LOGGER_HPP_
#define _LOGGER_HPP_

#include <iostream>
#include <sstream>
#include <map>

#define LOG(severity) \
    logger::Message().stream(logger::severity)

#define LOGTHROTTLE(severity, resource, interval) \
	logger::Message().stream(logger::severity, resource, interval)

#ifdef DEBUG 
#define DLOG(severity) LOG(severity)
#else
#define DLOG(severity) \
    true ? (void)0 : logger::StreamVoidifier() & LOG(severity)
#endif

namespace logger {

enum Severity {
    INFO    = 0,
    WARNING = 1,
    ERROR   = 2
};

class Message {
    public:
        Message() {}

        ~Message();

        std::ostringstream& stream(Severity l);
        std::ostringstream& stream(Severity l, std::string resource, long interval);
    private:
        Severity level;
        std::ostringstream os;
        std::ostringstream nul;
        
        Message(const Message&);
        Message& operator=(const Message&);
        static std::map<std::string, long> delays;

};

class StreamVoidifier {
    public:
        StreamVoidifier() {}
        void operator& (std::ostream&) {}
};

} // namespace logger

#endif
