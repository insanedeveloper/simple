#ifndef PRINTER_H
#define PRINTER_H

#include <fstream>

#include "Mesh.hpp"
#include "mesh/unstruct/Polygon.hpp"

#include <json/json.h>

class Printer {
	public:
		void print(int i,
                   Mesh& mesh);

		Printer(Json::Value& tree);

	private:
		std::string dirname, filename, functionfilename;
        bool save_func;
        int save_macro_point, save_func_freq;

		void saveMacroParams(int i,
                             Mesh& mesh);

		void saveSpeedFunction(int i,
                               Mesh& mesh);

};

#endif /*PRINTER_H*/
