#ifndef COLLIDE34534
#define COLLIDE34534

#include "stat.hpp"

class Collider{
public:
	Collider(Stat& stat, int flags){
		this->stat = &stat;
	}
	void collide(Mesh& mesh);
private:
	void collideWithin(Polygon* cell, int cellID, double timeStep);
	void absorption(Material* material, Momentum& dest, Momentum& src, EnergyDistribution& srcFlow, int cellID);
	void elastic(Material* material, Momentum& dest, Momentum& src, EnergyDistribution& srcFlow, int cellID);
	void inelastic(Material* material, Momentum& dest, Momentum& src, EnergyDistribution& srcFlow, int cellID);
	Stat* stat;
};
#endif
