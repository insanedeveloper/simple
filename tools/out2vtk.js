#!/usr/bin/env node
var nopt = require('nopt'),
    path = require('path'),
    fs = require('fs'),
    opts = nopt({
        "mesh": path,
        "pattern": path
    });
    if(!opts.mesh || !opts.pattern){
        console.log('--mesh <file>  - path to json mesh file\n--pattern <file>  - filename pattern of out files');
        return;
    }
var mesh = readMesh(opts.mesh);
var filelist = getFileList(opts.pattern);
console.log('Нашёл ' + filelist.length + ' файлов, удовлетворяющих шаблону "' + opts.pattern + '"\n', filelist);
filelist.forEach(function(file){
    fs.readFile(file, 'utf-8', function(err, data){
        if(err){
            console.log('Не прочитать файл "' + file + '":', err);
            return;
        }
        var json;
        try{
            json = JSON.parse(data);
        }catch(e){
            console.log('Не распарсить файл "' + file + '":', e);
            return;
        }
        makeVtkFile(json, mesh, file);
    });
});
/*-------------*/
function readMesh(filename){
    var json = JSON.parse(fs.readFileSync(filename,'utf-8')),
    nodes = new Buffer(json.nodes, 'base64'),
    array = new ArrayBuffer(nodes.length),
    uint = new Uint8Array(array);
    for(var i = 0; i < nodes.length; i++){
        uint[i] = nodes[i];
    }
    delete uint;
    delete nodes;
    var doubles = new Float64Array(array);
    json.doubles = doubles;
    return json;
}

function getFileList(pattern){
    var dir = path.dirname(pattern),
        p = path.basename(pattern),
        files = fs.readdirSync(dir),
        re = new RegExp(p, 'i');
        match = [];
    for(var i = 0; i < files.length; i++){
        if(re.test(files[i])){
            match.push(path.join(dir,files[i]));
        }
    }
    return match;
}

function makeVtkFile(data, mesh, file){
    var res = '<VTKFile type="UnstructuredGrid"><UnstructuredGrid>\n';
    var numberOfPoints = mesh.doubles.length/3;
    var numberOfCells = mesh.cells.length;
    res += '<Piece NumberOfPoints="' + numberOfPoints + '" NumberOfCells="' + numberOfCells + '">\n';
    res += getCellData(data, numberOfCells) + '\n';
    res += '<Points><DataArray NumberOfComponents="3" format="ascii" type="Float64">' + floatArrayToStr(mesh.doubles) + '</DataArray></Points>\n';
    res += '<Cells>' + getCells(mesh) + '</Cells>\n';
    res += '</Piece></UnstructuredGrid></VTKFile>';
    fs.writeFile(file + '.vtu', res, 'utf-8', function(err){
        if(err){
            throw err;
        }
    });
}
function floatArrayToStr(a){
    return (Array.apply([], a)).join(' ');
}
function flatten(obj){
    if(typeof obj !== 'object' || obj instanceof Array){
        return obj;
    }
    for(var key in obj){
        var prop = obj[key];
        if(typeof prop === 'object' && !(prop instanceof Array)){
            prop = flatten(prop); 
            for(var subkey in prop){
                obj[key + '_' + subkey] = prop[subkey];
            }
            obj[key] = undefined;
        }
    }
    return obj;
}
function getCellData(data, cellCount){
    var scalars = {},
        vectors = {},
        props = [],
        vprops = [],
        str = '';
    for(var key in data){
        var cell = flatten(data[key]),
            id = parseInt(key,10);
        if(typeof cell === 'object'){
            for(var prop in cell){
                var cellProp = cell[prop];
                if(cellProp instanceof Array){
                    if(!vectors[prop]){
                        vectors[prop] = new Array(cellCount);
                        vprops.push(prop);
                    }
                    if(!vectors[prop][id]){
                        vectors[prop][id] = [];
                    }
                    vectors[prop][id].push(cellProp[0],cellProp[1],cellProp[2]);
                }else if(typeof cellProp === 'number'){
                    if(!scalars[prop]){
                        scalars[prop] = new Array(cellCount);
                        props.push(prop);
                    }
                    scalars[prop][id] = cellProp;
                }
            }
        }
    }

    for(var key in scalars){
        str += getDataArray(scalars[key], "Float64", key) + '\t\n';
    }
    for(var key in vectors){
        str += getVectorDataArray(vectors[key], "Float64", key) + '\t\n';
    }
    return '<CellData Scalars="' + props.join(',') + '" Vectors="' + vprops.join(',') + '">' + str + '</CellData>';
}
function getCells(mesh){
    var connectivity = [],
        offsets = [],
        types = [],
        cells = mesh.cells,
        offset = 0,
        getType = {
            4 : 10,
            6 : 13
        };
    for(var i = 0; i < cells.length; i++){
        var cell = cells[i];
        for(var c = 0; c < cell.nodes.length; c++){
            connectivity[offset++] = cell.nodes[c];    
        }
        offsets[i] = offset;
        types[i] = getType[cell.type];
        if(!types[i]){
            console.warn('Unknown type:', cell.type);
        }
    }

    return getDataArray(connectivity, 'Int32', 'connectivity') +
    getDataArray(offsets, 'Int32', 'offsets') +
    getDataArray(types, 'UInt8', 'types') ;
 
}
function getVectorDataArray(data, type, name){
    for(var i = 0; i < data.length; i++){
        if(!data[i]){
            data[i] = [0, 0, 0];
        }
        data[i] = data[i].join(' ');
    }
    return '<DataArray type="' + type + '" Name="' + name + '" NumberOfComponents="3">' + data.join(' ') + '</DataArray>';
}
function getDataArray(data, type, name){
    for(var i = 0; i < data.length; i++){
        if(!data[i]){
            data[i] = 0;
        }
    }
    return '<DataArray type="' + type + '" Name="' + name + '">' + data.join(' ') + '</DataArray>';
}