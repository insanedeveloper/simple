module.exports = {
    extractExactCells: function(data, cellsList, dataParam){
        var res = {};
        cellsList.forEach(function(id){
            res[id] = dataParam? this[id][dataParam]:this[id];
        }, data);
        return res;
    }
}