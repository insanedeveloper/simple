 /* global $ */
 function logTicks(axis){
    var ticksCount = 10;
        emin = Math.log(axis.min),
        emax = Math.log(axis.max),
        delta = (emax - emin)/(ticksCount + 1),
        ticks = [];
    for(var i = 1; i <= ticksCount; i++ ){
        ticks[i] = Math.exp(emin + i*delta);
    }
    return ticks;
}

var wrap = $('.plot-wrap'),
    plot = $('.plot'),
    options = {
        xaxis: {
            transform: Math.log,
            inverseTransform: Math.exp,
            ticks: logTicks,
            tickFormatter: function(value){
                return value.toExponential(2);
            }
        },
        /*yaxis: {
            transform: Math.log,
            inverseTransform: Math.exp,
            ticks: logTicks,
            tickFormatter: function(value, axis){
                return value.toExponential(2);
            },
            min: 1E-1,
            max: 1E2
        },*/
        series: {
            lines: {
                fill: 0.05
            }
        },
        grid: {
            margin: {
                bottom: 40
            }
        },
        zoom: {
            interactive: true
        },
        pan: {
            interactive: false,
            cursor: 'move',      // CSS mouse cursor value used when dragging, e.g. "pointer"
            frameRate: 20
        }
    },
    JSONdata,
    seriesData,
    flot;

$('.file').change(function(e){
    var f = e.target.files[0];
    var reader = new FileReader();
    reader.onload = function(e){
        JSONdata = JSON.parse(e.target.result);
        $(window).trigger('loaded');
    };
    reader.readAsText(f, 'utf-8');
});

$(window).resize(resize);
resize();
function resize(){
    var dimensions = {
        width: wrap.width(),
        height: wrap.height()
    };
    plot.css(dimensions);
}
$('.click-me').on('click', function(){
    $(window).trigger('do_redraw');
    doRedraw();
});
function doRedraw(){
    flot = $.plot(plot, seriesData, options);
}
$('input[name="apply-scales"]').on('click', function(){
    options.xaxis = getAxisProps('xaxis');
    options.yaxis = getAxisProps('yaxis');
    doRedraw();
});
function setData(data){
    seriesData = data;
}
function getAxisProps(axisName){
    var limits = {'min': null, 'max': null};
    for(var id in limits){
        var val = $('input[name="' + axisName + id + '"]').val();
        if(val && val !== 'auto'){
            limits[id] = Number(val);
        }
    }
    var opts = {};
    if($('input[name="xaxislog"]').prop('checked')){
        opts = {
            transform: Math.log,
            inverseTransform: Math.exp,
            ticks: logTicks,
            tickFormatter: function(value){
                return value.toExponential(2);
            }
        };
    }
    opts.min = limits.min;
    opts.max = limits.max;
    return opts;
}