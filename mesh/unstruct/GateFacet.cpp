#include "GateFacet.hpp"

#include "FacetFactory.hpp"


void GateFacet::doTransfer(std::vector<Polygon*>& spacemesh)
{
    size_t vCount = Momentum::getVectorCount();

    Polygon* left = spacemesh[polygon[0]];
    Polygon* right = spacemesh[polygon[1]];

    Momentum& f1_left = left->f();
    Momentum& f2_left = left->g();

    Momentum& f1_right = right->f();
    Momentum& f2_right = right->g();

    for (size_t i = 0; i < vCount; ++i) {
        double sppr = Momentum::dotProduct(i, n);
        EnergyDistribution& f1_group = (sppr < 0.0? f1_left:f1_right).getGroupForVector(i);

        EnergyDistribution& f2_left_group = f2_left.getGroupForVector(i);
        EnergyDistribution& f2_right_group = f2_right.getGroupForVector(i);

        if(!left->isFrozen()){
            f2_left_group.multAndAdd(f1_group, sppr*mult_left);
        }
        if(!right->isFrozen()){
            f2_right_group.multAndSub(f1_group, sppr * mult_right);
        }
    }
}
/**
 * TODO: выпилить DistributionFunction
 */
/*void GateFacet::doTransfer2(std::vector<Polygon*>& spacemesh)
{
	size_t size = Momentum::getVectorCount();

	Polygon* left = spacemesh[polygon[0]];
	Polygon* right = spacemesh[polygon[1]];

    Momentum& f1_left = left.f();
    Momentum& f2_left = left.g();

    Momentum& f1_right = right.f();
    Momentum& f2_right = right.g();

    SpeedFunction& f1 = spacemesh[polygon[0]]->f();

    const DistributionFunction3& df_in     = f1.getGradient();
    const DistributionFunction&  phi_in    = f1.getPhi();

    SpeedFunction& f2 = spacemesh[polygon[1]]->f();

    const DistributionFunction3& df_out    = f2.getGradient();
    const DistributionFunction&  phi_out   = f2.getPhi();

    V3d d_out = getCenter() - spacemesh[polygon[1]]->getCenter();
    
    for (size_t i = 0; i < size; ++i) {
        double sppr = Momentum::dotProduct(i, n);
        if(sppr > 0.0) {
            double dd = - 0.5 * dt * dot(vels[i], df_in[i]);
//            double dd = 0.0;
            double d = (f1_in[i] + (dot(df_in[i], d_in) + dd)*phi_in[i])*sppr;
            f2_in[i] += d * mult_left;
            f2_out[i] -= d * mult_right;
        }
        else{
            double dd = - 0.5 * dt * dot(vels[i], df_out[i]);
//            double dd = 0.0;
            double d = (f1_out[i] + (dot(df_out[i], d_out) + dd)*phi_out[i])*sppr;
            f2_in[i] += d * mult_left;
            f2_out[i] -= d * mult_right;
        }
    }
}
*/
void GateFacet::calculateDistance(std::vector<Polygon*>& spacemesh) 
{
    Polygon* p_in  = spacemesh[polygon[0]];
    Polygon* p_out = spacemesh[polygon[1]];

    V3d d = p_out->getCenter() - p_in->getCenter();
    d_in  = getCenter() - p_in->getCenter();
    d_out = getCenter() - p_out->getCenter();

//  std::cout << "ds = " << d << std::endl;

    T3d dd = prod(d, d);
    p_in->getDD() += dd;
    p_out->getDD() += dd;
}
/*
void GateFacet::doFindGradient(const std::vector<Polygon*>& spacemesh)
{
    size_t size       = spacemesh[polygon[0]]->f().size();

    SpeedFunction& f1 = spacemesh[polygon[0]]->f();
    const DistributionFunction& f_in = f1.f();
    DistributionFunction3& df_in     = f1.getGradient();
    DistributionFunction& fmin_in    = f1.getFMin();
    DistributionFunction& fmax_in    = f1.getFMax();

    SpeedFunction& f2 = spacemesh[polygon[1]]->f();
    const DistributionFunction& f_out = f2.f();
    DistributionFunction3& df_out     = f2.getGradient();
    DistributionFunction& fmin_out    = f2.getFMin();
    DistributionFunction& fmax_out    = f2.getFMax();

    V3d d_in_out = d_in - d_out;
    
    for (size_t i = 0; i < size; ++i) {
        V3d b = (f_out[i] - f_in[i]) * d_in_out;
        df_in[i] += b;
        df_out[i] += b;

        if (fmin_in[i] > f_out[i])
            fmin_in[i] = f_out[i];
        if (fmax_in[i] < f_out[i])
            fmax_in[i] = f_out[i];
        if (fmin_out[i] > f_in[i])
            fmin_out[i] = f_in[i];
        if (fmax_out[i] < f_in[i])
            fmax_out[i] = f_in[i];

    }
}
*/
REGISTER_FACET(GateFacet, "gate")
