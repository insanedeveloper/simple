#ifndef POLYGON_H_
#define POLYGON_H_

#include <string>
#include <vector>

#include "../CellBase.hpp"
#include "../../momentum.hpp"
#include "../../materials/material.hpp"

#include "../../v.hpp"

enum FrozenState{
    FROZEN_STATE_NORMAL = 0,
    FROZEN_STATE_MELTED,
    FROZEN_STATE_FROZEN
};

class Polygon : public CellBase {
    protected:

        int numberOfVertex;
        int numberOfEdges;
        int numberOfFacets;

        double V;
        double Lmin;
        double density;
        Material* material;
        V3d center;             // necessary just for second order
        T3d dd;

        std::vector<V3d> vertex;

        Momentum current;
        Momentum next;

        FrozenState frozen;
        double deltaCache;

        static double minDelta;

    public:
        void calculateLength();
        virtual void calculateVolume() = 0;
        virtual void calculateCenter() = 0;

        void setVertexes(const std::vector<V3d>& vertexes) { vertex = vertexes; }

        double getLMin() const { return Lmin; }
        int getNumberOfVertexes() const { return  numberOfVertex; }
        
        void setRank(int rank_) { rank = rank_; }
        int getRank() const { return rank; }

        double getVolume() const { return V; }
        V3d getCenter() const { return center; }
        T3d& getDD() { return dd; } 

        void inverseDD();

        inline bool isFrozen(){
            return frozen == FROZEN_STATE_FROZEN;
        }
        inline bool isMelted(){
            return frozen == FROZEN_STATE_MELTED;
        }
        inline bool isNormal(){
            return frozen == FROZEN_STATE_NORMAL;
        }
        inline void markAsMelted(){
            frozen = FROZEN_STATE_MELTED;
        }
        inline static void setMinDelta(double delta){
            Polygon::minDelta = delta;
        }
        inline void reset(){
            deltaCache = 0;
        }
        inline double getDelta(){
            if(isFrozen()){
                return deltaCache;
            }else{
                return next.getFlow().totalFlow();
            }
        }
/*
        void prepareForNextStep();
        void findGradientAndPhi();
*/
        void setDensity(double density){
        	this->density = density;
        }
        EnergyDistribution getLeakage();
        inline double getDensity() { return density; }

        void setMaterial(Material* material){
        	this->material = material;
        }
        inline Material* getMaterial() { return material; }

        inline Momentum& f() { return current; }
        inline Momentum& g() { return next; }
    	inline void iterate(){
    	    if(isNormal()){
    	        deltaCache = next.getFlow().totalFlow();
    	    }else if (isMelted()){
    	        deltaCache += next.getFlow().totalFlow();
    	    }
    	    if(deltaCache <= Polygon::minDelta){
    	        frozen = FROZEN_STATE_FROZEN;
    	    }else{
    	        frozen = FROZEN_STATE_NORMAL;
    	        deltaCache = 0;
    	    }
    	    current += next;
    	}
    	inline void newStep(){
    		next.clear();
    	}
    	inline EnergyDistribution getDensityTimesTotalCrossSection(){
    		if(!material){
    			return 0;
    		}
    		return material->total * density;
    	}
};

#endif /* POLYGON_H_ */

