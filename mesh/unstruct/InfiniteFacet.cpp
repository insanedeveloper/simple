#include "InfiniteFacet.hpp"
#include "../../momentum.hpp"
#include "FacetFactory.hpp"

void InfiniteFacet::calculateDistance(std::vector<Polygon*>& spacemesh)
{
	Polygon* p_in = spacemesh[polygon[0]];

	d_in = 2.*(getCenter() - p_in->getCenter());

//	std::cout << "d_in (mirror) = " << d_in << std::endl;

	T3d dd = prod(d_in, d_in);
	p_in->getDD() += dd;
}


void InfiniteFacet::doTransfer(std::vector<Polygon*>& spacemesh)
{
/*
 * Просто ничего не делать.
 */
}
/*
void InfiniteFacet::doTransfer2(std::vector<Polygon*>& spacemesh)
{

}
*/
REGISTER_FACET(InfiniteFacet, "infinite")
