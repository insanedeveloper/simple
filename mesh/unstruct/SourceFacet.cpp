#include "SourceFacet.hpp"

#include "FacetFactory.hpp"

void SourceFacet::doTransfer(std::vector<Polygon*>& spacemesh)
{
    size_t vCount = Momentum::getVectorCount();

    Polygon* left = spacemesh[polygon[0]];

    if(left->isFrozen()){
        return;
    }

    Momentum& f1_left = left->f();
    Momentum& f2_left = left->g();
    Momentum& f1_src = src->getDistr();

    for (size_t i = 0; i < vCount; ++i) {

        double sppr = Momentum::dotProduct(i, n);

        EnergyDistribution& f2_left_group = f2_left.getGroupForVector(i);

        if(sppr < 0.0) {
        	EnergyDistribution& f1_left_group = f1_left.getGroupForVector(i);
        	f2_left_group.multAndAdd(f1_left_group, sppr*mult_left);
		}else{
			EnergyDistribution& f1_src_group = f1_src.getGroupForVector(i);
			f2_left_group.multAndAdd(f1_src_group, sppr*mult_left);
        }
    }
}

void SourceFacet::calculateDistance(std::vector<Polygon*>& spacemesh)
{
    Polygon* p_in  = spacemesh[polygon[0]];
    Polygon* p_out = spacemesh[polygon[1]];

    V3d d = p_out->getCenter() - p_in->getCenter();
    d_in  = getCenter() - p_in->getCenter();
    d_out = getCenter() - p_out->getCenter();


    T3d dd = prod(d, d);
    p_in->getDD() += dd;
    p_out->getDD() += dd;
}

REGISTER_FACET(SourceFacet, "source")
