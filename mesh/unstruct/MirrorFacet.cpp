#include "MirrorFacet.hpp"
#include "../../momentum.hpp"
#include "FacetFactory.hpp"


map<V, int*, CompareV> MirrorFacet::transitionTableCache;

MirrorFacet::~MirrorFacet(){
	map<V, int*, CompareV>::iterator end = MirrorFacet::transitionTableCache.end();
	map<V, int*, CompareV>::iterator ptr = MirrorFacet::transitionTableCache.find(n);
	if(ptr != end){
		delete[] ptr->second;
		MirrorFacet::transitionTableCache.erase(ptr);
	}
	transitionTable = 0;
}

void MirrorFacet::initTransitionTable(){
	int*& cachedTable = transitionTableCache[n];
	if(cachedTable != 0){
		transitionTable = cachedTable;
		return;
	}
	int vCount = Momentum::getVectorCount();
	cachedTable = transitionTable = new int[vCount];
	for(int i = 0; i < vCount; i++){
		V v = Momentum::getVector(i);
		v -= 2 * dot(v,n) * n;
		transitionTable[i] = Momentum::findClosestFor(v);
	}
}

void MirrorFacet::calculateDistance(std::vector<Polygon*>& spacemesh)
{
	Polygon* p_in = spacemesh[polygon[0]];

	d_in = 2.*(getCenter() - p_in->getCenter());

//	std::cout << "d_in (mirror) = " << d_in << std::endl;

	T3d dd = prod(d_in, d_in);
	p_in->getDD() += dd;
}


void MirrorFacet::doTransfer(std::vector<Polygon*>& spacemesh)
{
	size_t vCount = Momentum::getVectorCount();

	Polygon* left = spacemesh[polygon[0]];

	if(left->isFrozen()){
        return;
    }
	Momentum& f1_left = left->f();
	Momentum& f2_left = left->g();

	for (size_t i = 0; i < vCount; ++i) {

	    double sppr = Momentum::dotProduct(i, n);
	    if(sppr < 0.0) {
	    	EnergyDistribution& f1_left_group = f1_left.getGroupForVector(i);
	    	EnergyDistribution& f2_left_group = f2_left.getGroupForVector(i);
	    	EnergyDistribution& f2_mirroredleft_group = f2_left.getGroupForVector(transitionTable[i]);
	    	f2_left_group.multAndAdd(f1_left_group, sppr*mult_left);
	    	f2_mirroredleft_group.multAndSub(f1_left_group, sppr*mult_left);
		}
	}
}
/*
void MirrorFacet::doTransfer2(std::vector<Polygon*>& spacemesh)
{
    size_t size       = spacemesh[polygon[0]]->f().size();

    SpeedFunction& f1 = spacemesh[polygon[0]]->f();
    DistributionFunction&  f1_in       = f1.f();
    DistributionFunction&  f2_in       = f1.g();
    const DistributionFunction3& df_in = f1.getGradient();
    const DistributionFunction& phi_in = f1.getPhi();

	V3d d_in = getCenter() - spacemesh[polygon[0]]->getCenter();

    for (size_t i = 0; i < size; ++i) {
		double sppr = Momentum::dot(i, n);
		if (sppr < 0.0) {
            double dd = - 0.5 * dt * gas.dot(i, df_in[i]);
			f2_in[i] += (f1_in[i] + (dot(df_in[i], d_in)+dd)*phi_in[i])*sppr*mult_left;
        }
		else {
            double dd = - 0.5 * dt * gas.dot(i, df_in[gas.mirror(i, symm)]);
			f2_in[i] += (f1_in[gas.mirror(i, symm)] + 
					(dot(df_in[gas.mirror(i, symm)], d_in) + dd) * 
					    phi_in[gas.mirror(i, symm)]) * sppr * mult_left;
        }
	}
}
*/
REGISTER_FACET(MirrorFacet, "mirror")
