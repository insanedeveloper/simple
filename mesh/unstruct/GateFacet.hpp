#ifndef GATEFACET_H
#define GATEFACET_H

#include "PhysicalFacet.hpp"

class GateFacet : public PhysicalFacet {
	public:
        void init(Json::Value& tree) {}

		void calculateDistance(std::vector<Polygon*>& spacemesh);

		void doTransfer(std::vector<Polygon*>& spacemesh);
		//void doTransfer2(std::vector<Polygon*>& spacemesh);

		//void doFindGradient(const std::vector<Polygon*>& spacemesh);

		int order() const { return 0; }
};

#endif /*GATEFACET_H*/
