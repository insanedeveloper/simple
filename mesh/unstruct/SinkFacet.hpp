#ifndef SINKFACET_H
#define SINKFACET_H

#include "PhysicalFacet.hpp"

class SinkFacet : public PhysicalFacet {
	public:
	SinkFacet()  {}

		void init(Json::Value& tree) {}

		void doTransfer(std::vector<Polygon*>& spacemesh);
		//void doTransfer2(std::vector<Polygon*>& spacemesh);
		void calculateDistance(std::vector<Polygon*>& spacemesh);
		//void doFindGradient(const std::vector<Polygon*>& spacemesh);

		int order() const { return 0; }
};

#endif /*SINKFACET_H*/
