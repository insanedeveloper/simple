#include <limits>
#include <algorithm>
#include <fstream>
#include <json/json.h>
#include "UnstructMesh.hpp"

#include "Tetrahedron.hpp"
#include "Prism.hpp"
#include "Hexahedron.hpp"

#include "FacetFactory.hpp"

#include "../../source.hpp"
#include "../../base64/base64.hpp"
#include "../../logger/logger.hpp"

Polygon* createPolygon(Json::Value& celldata) {
    int type = celldata["type"].asInt();
    if      (type == 4) return new Tetrahedron();
    else if (type == 5) return new Hexahedron();
    else if (type == 6) return new Prism();
    else return 0;
}

template <typename Element>
void constructElement(Json::Value& elemdata,
                      const std::vector<V3d>& nodes, 
                      Element* elem)
{
    Json::Value& ns = elemdata["nodes"];
    std::vector<V3d> vertexes(ns.size());
    for (Json::ArrayIndex i = 0; i < (Json::ArrayIndex)vertexes.size(); ++i) 
        vertexes[i] = nodes[ns[i].asInt()];

    Json::Value& nb = elemdata["neigbors"];
    std::vector<int> neigbors(nb.size());
    for (Json::ArrayIndex i = 0; i < (Json::ArrayIndex)neigbors.size(); ++i) 
        neigbors[i] = nb[i].asInt();

    elem->setVertexes(vertexes);
    elem->setNeigbors(neigbors);
}

Polygon* constructPolygon(Json::Value& celldata,
                      const std::vector<V3d>& nodes,
                      Json::Value& composition)
{
    Polygon* polygon = createPolygon(celldata);

    constructElement(celldata, nodes, polygon);

    polygon->calculateLength();
    polygon->calculateVolume();
    polygon->calculateCenter();    

    int rank = celldata["part_index"].asInt();
    std::string phys_name = celldata["phys_name"].asString();

    if(composition.isMember(phys_name)){
    	Json::Value& member = composition[phys_name];
        polygon->setDensity(member["density"].asDouble());
        if(!member.isMember("material")){
        	LOG(ERROR) << "No material name in composition '" << phys_name << "'";
        }
        Material* mat = Material::getMaterialById(member["material"].asString());
        if(!mat){
        	LOG(ERROR) << "No material found with name '" << member["material"].asString() << "'";
        }
        polygon->setMaterial(mat);
    }else{
    	polygon->setDensity(0);
    	polygon->setMaterial(0);
    }

    polygon->setRank(rank);
    polygon->setPhysicalName(phys_name);

    return polygon;
}

PhysicalFacet* constructFacet(Json::Value& facetdata,
                              const std::vector<V3d>& nodes,
                              std::vector<Polygon*>& polygons)
{
    std::string phys_name = facetdata["phys_name"].asString();
    std::string  facet_name = "gate";
    Json::Value& facet_cond = facetdata;

    if (phys_name == "mirror") {
        facet_name = "mirror";
    }else if(phys_name == "infinite"){
    	facet_name = "infinite";
    }else if(facetdata["neigbors"].size() == 1){
    	facet_name = "sink";
    }

    if(Source::getByName(phys_name)){
    	facet_name = "source";
    }
    PhysicalFacet* facet = FacetFactory::instance().create(facet_name);
    constructElement(facetdata, nodes, facet);
    facet->setType(facetdata["type"].asInt());
    facet->findNormalAndSquareAndMult(polygons);
    facet->init(facet_cond);
    return facet;
}


bool lessFacet(PhysicalFacet* f1, PhysicalFacet* f2) {
    return f1->order() < f2->order();
}

void ElementsConstructor(Json::Value& tree,
                         std::vector<Polygon*>& polygons,
                         std::vector<PhysicalFacet*>& facets)
{
	std::string meshFile = tree["mesh"].asString();
	fstream meshFileStream;
	meshFileStream.open(meshFile.c_str(), ios::in);
	Json::Reader reader;
    Json::Value meshdata;
	bool parsingSuccessful = reader.parse( meshFileStream, meshdata );
	meshFileStream.close();
	if ( !parsingSuccessful ){
	    // report to the user the failure and their locations in the document.
		 LOG(ERROR)  << "Failed to parse mesh file '"<< meshFile <<"'\n"
	               << reader.getFormatedErrorMessages();
	    return;
	}

    Json::Value& compositionData  = tree["composition"];

    // construct nodes
    std::vector<unsigned char> nodesbytes = base64::decode(meshdata["nodes"].asString());
    const double* nodesdoubles = reinterpret_cast<const double*>(&nodesbytes.front());
    size_t nodes_num = meshdata["nodes_num"].asInt();
    LOG(INFO) << "nodes_num = " << nodes_num;
    std::vector<V3d> nodes(nodes_num);
    for (size_t i = 0, j = 0; i < nodes.size(); ++i) {
        double x = nodesdoubles[j++];
        double y = nodesdoubles[j++];
        double z = nodesdoubles[j++];
//      std::cout << x << ' ' << y << ' ' << z << std::endl;
        nodes[i] = V3d(x, y, z);
    }

    // construct cells
    Json::Value& cellsdata = meshdata["cells"];
    for (Json::ArrayIndex i = 0; i < (Json::ArrayIndex)cellsdata.size(); ++i) 
        polygons.push_back(constructPolygon(cellsdata[i], nodes, compositionData));
    LOG(INFO) << "cells.size() = " << polygons.size();

    // construct facets
    Json::Value& facetsdata = meshdata["facets"];
    for (Json::ArrayIndex i = 0; i < (Json::ArrayIndex)facetsdata.size(); ++i)
        facets.push_back(constructFacet(facetsdata[i], nodes, polygons));

    std::sort(facets.begin(), facets.end(), lessFacet);
    LOG(INFO) << "facets.size() = " << facets.size();

}

double findTimeStep(const std::vector<Polygon*>& cells,
		const std::vector<PhysicalFacet*>& facets,
        double curnt)
{
	double multMax = 0;
	for (size_t i = 0; i < facets.size(); ++i){
		double m = facets[i]->getMaxMult();
		if(multMax < m){
			multMax = m;
		}
	}

    double timeStep = curnt / multMax;
    LOG(INFO) << "Max Mult = " << multMax << ", timestep = " << timeStep;

	double maxdd = 0;
	for (size_t i = 0; i < cells.size(); i++){
		Polygon* cell = cells[i];
		EnergyDistribution d = cell->getDensityTimesTotalCrossSection() * timeStep;

		for(size_t g = 0; g < EnergyDistribution::getGroupCount(); g++){
			if(maxdd < d[g]){
				maxdd = d[g];
			}
		}
	}
	LOG(INFO) << "Max dd = " << maxdd <<", expdd = " << 1 - exp(-maxdd);
	LOG(INFO) << " dd = " << maxdd <<", expdd = " << 1 - exp(-maxdd);
	double targetTS = 0.2/maxdd;
	if(targetTS < timeStep){
		LOG(INFO) << "timestep shift: " << timeStep << " - " << targetTS;
		timeStep = targetTS;
	}
    return timeStep;
}

UnstructMesh::UnstructMesh(Json::Value& tree)
{
    ElementsConstructor(tree, cells, facets);

    double curnt = tree["courantNumber"].asDouble();
    LOG(INFO) << "curnt = " << curnt;

    time_step = findTimeStep(cells, facets, curnt);
    LOG(INFO) << "time_step = " << time_step;
    
    for (std::vector<PhysicalFacet*>::iterator pp = facets.begin();
            pp != facets.end(); ++pp)
        (*pp)->rescaleMults(time_step);
}

