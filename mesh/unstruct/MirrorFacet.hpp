#ifndef MIRRORFACET_H
#define MIRRORFACET_H

#include "PhysicalFacet.hpp"
class CompareV{
public:
	bool operator ()(const V f, const V s){
		for(int i = 0; i < 3; i++){
			if(f[i] < s[i]){
				return false;
			}
			if(f[i] > s[i]){
				return true;
			}
		}
		return false;
	}
};

class MirrorFacet : public PhysicalFacet {
	public:
		~MirrorFacet();

		void init(Json::Value& tree) {
        	initTransitionTable();
        }

		void doTransfer(std::vector<Polygon*>& spacemesh);
		//void doTransfer2(std::vector<Polygon*>& spacemesh);
		void calculateDistance(std::vector<Polygon*>& spacemesh);
		//void doFindGradient(const std::vector<Polygon*>& spacemesh);

		int order() const { return 0; }
	private:
		void initTransitionTable();
		int* transitionTable;
		static map<V, int*, CompareV> transitionTableCache;
};

#endif /*MIRRORFACET_H*/
