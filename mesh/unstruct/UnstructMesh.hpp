#ifndef _UNSTRUCTMESH_HPP_
#define _UNSTRUCTMESH_HPP_

#include <vector>

#include "../MeshBase.hpp"
#include "../../auxiliary.hpp"
#include <json/json.h>

class UnstructMesh : public MeshBase {
    public:
        UnstructMesh(Json::Value& tree);

        virtual Cells&  getCells()  { return cells;  }
        virtual Facets& getFacets() { return facets; }

        virtual double getTimeStep() const {
            LABEL
            return time_step;
        }

    private:
        Cells  cells;                   
        Facets facets;  

        double time_step;
};

#endif // _UNSTRUCTMESH_HPP_
