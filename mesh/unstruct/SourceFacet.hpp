#ifndef SOURCEFACET_H
#define SOURCEFACET_H

#include "PhysicalFacet.hpp"
#include "../../source.hpp"
class SourceFacet : public PhysicalFacet {
	public:
		SourceFacet(){
			type = numberOfVertex = 0;
			S = mult_left = mult_right = dt = 0;
			d_in = d_out = 0;
			src = 0;
		}
		~SourceFacet(){
			delete[] src;
		}
        void init(Json::Value& data) {
        	this->src = Source::getByName(data["phys_name"].asString());
        }

		void calculateDistance(std::vector<Polygon*>& spacemesh);

		void doTransfer(std::vector<Polygon*>& spacemesh);

		int order() const { return 0; }
	private:
		Source* src;
};

#endif /*SOURCEFACET_H*/
