#include "PhysicalFacet.hpp"
#include "math.h"

inline V3d triangleCenter(V3d p1, V3d p2, V3d p3) {
    return (p1 + p2 + p3) / 3.;
}

inline double triangleVolume(V3d p1, V3d p2, V3d p3) {
    return 0.5 * norm( cross(p2 - p1, p3 - p1) );
}

void PhysicalFacet::findNormalAndSquareAndMult(const std::vector<Polygon*>& spacemesh)
{
    n = cross(vertex[1] - vertex[0], vertex[2] - vertex[0]);
    n /= norm(n);
    if (type == 2) {
        S = triangleVolume(vertex[0], vertex[1], vertex[2]);
        center = triangleCenter(vertex[0], vertex[1], vertex[2]);
    }
    else if (type == 3) {
        double s1 =     triangleVolume(vertex[0], vertex[1], vertex[2]);
        V3d c1 =        triangleCenter(vertex[0], vertex[1], vertex[2]);
        double s2 =     triangleVolume(vertex[0], vertex[2], vertex[3]);
        V3d c2 =        triangleCenter(vertex[0], vertex[2], vertex[3]);
        S = s1 + s2;
        center = (c1 * s1 + c2 * s2) / (s1 + s2);
    }
    mult_left = S / spacemesh[polygon[0]]->getVolume();
    if (polygon.size() > 1){
        mult_right = S / spacemesh[polygon[1]]->getVolume();
    }
}

void PhysicalFacet::setType(int type_)
{
    type = type_;
    if(type == 2) numberOfVertex = 3;
    if(type == 3) numberOfVertex = 4;
}

void PhysicalFacet::setNeigbors(const std::vector<int>& neigbors)
{
    polygon = neigbors;
}

void PhysicalFacet::setVertexes(const std::vector<V3d>& vertexes)
{
    vertex = vertexes;
}

void PhysicalFacet::rescaleMults(double timeStep) {
    dt = timeStep;
    mult_left *= timeStep;
    if (polygon.size() > 1)
        mult_right *= timeStep;
    if(mult_left > 1){
    	LOG(WARNING) << "mult_left more than 1: " << mult_left;
    }
    if(mult_right > 1){
        LOG(WARNING) << "mult_right more than 1:" << mult_right;
    }

}
void PhysicalFacet::checkAndMarkAsMelted(const std::vector<Polygon*>& spacemesh){
    Polygon* left = 0;
    Polygon* right = 0;
    size_t count = polygon.size();
    if(count == 0){
        return;
    }

    left = spacemesh[polygon[0]];

    if(count > 1){
        right = spacemesh[polygon[1]];
    }
    /* melted или frozen - всё равно */
    bool frozen = !left->isNormal();
    if(right){
        frozen = frozen && !right->isNormal();
    }

    if(!frozen){
        if(right && !right->isNormal()){
            right->markAsMelted();
        }
        if(left && !left->isNormal()){
            left->markAsMelted();
        }
    }
}
void PhysicalFacet::findPhi(const std::vector<Polygon*>& spacemesh)
{
    //doFindPhi(spacemesh);
}
void PhysicalFacet::transfer(std::vector<Polygon*>& spacemesh)
{
    doTransfer(spacemesh);
}

void PhysicalFacet::transfer2(std::vector<Polygon*>& spacemesh)
{
    //doTransfer2(spacemesh);
}
/*void PhysicalFacet::findGradient(const std::vector<Polygon*>& spacemesh)
{
    doFindGradient(spacemesh);
}*/
/*
void PhysicalFacet::doFindPhi(const std::vector<Polygon*>& spacemesh)
{
    for (size_t j = 0; j < polygon.size(); ++j) {
        Polygon* poly = spacemesh[polygon[j]];
        SpeedFunction& function = poly->f();

        DistributionFunction&    func = function.f();
        DistributionFunction&    dd   = function.g();
        DistributionFunction&    fmax = function.getFMax();
        DistributionFunction&    fmin = function.getFMin();
        DistributionFunction3&  dfunc = function.getGradient();
        DistributionFunction&    fphi = function.getPhi();

        V3d cc = getCenter() - poly->getCenter();
        for(size_t i = 0; i < function.size(); i++) {
            V3d df = dfunc[i];
            double dl = dot(df, cc + dd[i] * dt);
            double d1 = fmax[i];
            double d2 = fmin[i];
            double phi;
            if (std::abs(dl) < 1e-10) phi = 0.;
            else {
                if (dl > 0) phi = std::min(1., d1 / dl);
                else        phi = std::min(1., d2 / dl);
            }
            if (fphi[i] > phi)
                fphi[i] = phi; 

        }
    }
}*/

