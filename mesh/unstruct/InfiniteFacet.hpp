#ifndef INFINITEFACET_H
#define INFINITEFACET_H

#include "PhysicalFacet.hpp"


class  InfiniteFacet : public PhysicalFacet {
	public:

		void init(Json::Value& tree) {}

		void doTransfer(std::vector<Polygon*>& spacemesh);
		//void doTransfer2(std::vector<Polygon*>& spacemesh);
		void calculateDistance(std::vector<Polygon*>& spacemesh);
		//void doFindGradient(const std::vector<Polygon*>& spacemesh);

		int order() const { return 0; }
};

#endif /*INFINITEFACET_H*/
