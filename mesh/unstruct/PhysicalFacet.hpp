#ifndef PHYSICALFACET_H_
#define PHYSICALFACET_H_

#include <vector>

#include "Polygon.hpp"

class PhysicalFacet {
	protected:
		int type;

		double S;
		V3d n, center;

		double mult_left, mult_right;
        double dt;

		int numberOfVertex;
		std::vector<V3d> vertex;
		std::vector<int> polygon;
		V3d d_in, d_out;

	public:
		PhysicalFacet(){
			type = numberOfVertex = 0;
			S = mult_left = mult_right = dt = 0;
			d_in = d_out = 0;
		}
		void setType(int type_);
		void setNeigbors(const std::vector<int>& neigbors_);
		void setVertexes(const std::vector<V3d>& vertex_);
		void findNormalAndSquareAndMult(const std::vector<Polygon*>& spacemesh);
		void rescaleMults(double timeStep);
		inline double getMaxMult(){
			return mult_left >  mult_right? mult_left : mult_right;
		}
		double getSquare() const { return S; }
		V3d getCenter() const { return center; }
        const std::vector<int>& getNeigbors() const { return polygon; }

        virtual void init(Json::Value& tree) = 0;

		void transfer(std::vector<Polygon*>& spacemesh);
		virtual void doTransfer(std::vector<Polygon*>& spacemesh) = 0;
		void transfer2(std::vector<Polygon*>& spacemesh);
        void checkAndMarkAsMelted(const std::vector<Polygon*>& spacemesh);
		//virtual void doTransfer2(std::vector<Polygon*>& spacemesh) = 0;

		void findPhi(const std::vector<Polygon*>& spacemesh);
		void doFindPhi(const std::vector<Polygon*>& spacemesh);

		virtual void calculateDistance(std::vector<Polygon*>& spacemesh) = 0;
/*
		void findGradient(const std::vector<Polygon*>& spacemesh);
		virtual void doFindGradient(const std::vector<Polygon*>& spacemesh) = 0;
*/
		static int getNumberOfVertexes(int type) {
			if (type == 2) return 3;
			else if (type == 3) return 4;
			else return 0;
		}
		virtual int order() const = 0;

	private:

};

#endif /* PHYSICALFACET_H_ */
