#ifndef _CELLBASE_H_
#define _CELLBASE_H_

#include <vector>

class CellBase {
    public:
        typedef std::vector<int> Ints;

        void setIndex(int i) { index = i; }
        int getIndex() const { return index; }

        void setNeigbors(const Ints& ns) { neigbors = ns; }
        const Ints& getNeigbors() const { return neigbors; }

        void setRank(int r) { rank = r; }
        int getRank() const { return rank; }

        void setPhysicalName(const std::string& name) { phys_name = name; }
        const std::string& getPhysicalName() const { return phys_name; }


    protected:
        int index;
        Ints neigbors;
        std::string phys_name;
        int rank;
};

#endif /* _CELLBASE_H_ */

