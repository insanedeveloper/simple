#include <iostream>
#include "Transfer1.hpp"

void Transfer1::move(Mesh& mesh){
    markAsMelted(mesh);

    for(size_t i = 0; i < mesh.getFlowingFacets().size(); i++){
        mesh.getFlowingFacets()[i]->transfer(mesh.getAllCells());
    }
}

void Transfer1::markAsMelted(Mesh& mesh){
    for (size_t i = 0; i < mesh.getFlowingFacets().size(); i++) {
        mesh.getFlowingFacets()[i]->checkAndMarkAsMelted(mesh.getAllCells());
    }
}
