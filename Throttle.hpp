#ifndef THROTT23423rf23
#define THROTT23423rf23

#include <string>
class Throttle {
public:
    Throttle(void(*func)(double a, double b, double c, int d, std::string e), long interval);
    void run(double a, double b, double c, int d, std::string e);
protected:
    long lastcall;
    long interval;
    void(*func)(double a, double b, double c, int d, std::string e);
};

#endif
