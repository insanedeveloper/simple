Point(1) = {0, 0, 0, 1.0};
Point(2) = {0, 10, 0, 1.0};
Point(5) = {0, 0, -10, 1.0};
Circle(4) = {5, 1, 2};
Point(6) = {20, 0, 0, 1.0};
Point(7) = {20, 10, 0, 1.0};
Point(10) = {20, 0, -10, 1.0};
Circle(8) = {10, 6, 7};
Line(9) = {2, 7};
Line(10) = {5, 10};
Point(11) = {-10, 0, 0, 1.0};
Point(12) = {-10, 20, 0, 1.0};
Point(15) = {-10, 0, -20, 1.0};
Point(16) = {100, 0, 0, 1.0};
Point(17) = {100, 100, 0, 1.0};
Point(20) = {100, 0, -100, 1.0};
Circle(24) = {20, 16, 17};
Circle(28) = {15, 11, 12};
Line(31) = {15, 20};
Line(32) = {17, 12};
Line(33) = {17, 16};
Line(34) = {16, 20};
Line(35) = {12, 11};
Line(36) = {11, 15};
Line(37) = {2, 1};
Line(38) = {1, 5};
Line(39) = {7, 6};
Line(40) = {6, 10};
Line(41) = {6, 1};
Line Loop(47) = {33, 34, 24};
Plane Surface(48) = {47};
Line Loop(49) = {35, 36, 28};
Plane Surface(50) = {49};
Line Loop(51) = {9, 39, 41, -37};
Plane Surface(52) = {51};
Line Loop(53) = {40, -10, -38, -41};
Plane Surface(54) = {53};
Line Loop(55) = {39, 40, 8};
Plane Surface(56) = {55};
Line Loop(57) = {37, 38, 4};
Plane Surface(58) = {57};
Line Loop(59) = {9, -8, -10, 4};
Ruled Surface(60) = {59};
Line Loop(61) = {31, 24, 32, -28};
Ruled Surface(62) = {61};
Surface Loop(64) = {60, 52, 56, 54, 58};
Volume(66) = {64};
Physical Volume("shield") = {66};
Physical Surface("source") = {50};
Line Loop(67) = {32, 35, 42, -33, 32, 35, 42, -33};
Line(68) = {16, 6};
Line(69) = {1, 11};
Line Loop(70) = {33, 68, -39, -9, 37, 69, -35, -32};
Plane Surface(71) = {70};
Line Loop(72) = {34, -31, -36, -69, 38, 10, -40, -68};
Plane Surface(73) = {72};
Surface Loop(74) = {71, 48, 73, 62, 50, 58, 60, 56};
Volume(75) = {74};
Physical Volume("void") = {75};
Physical Surface("mirror") = {73, 71, 52, 54};
Characteristic Length {17, 16, 20} = 10;
Characteristic Length {1, 2, 5, 10, 6, 7, 11, 15, 12} = 4;
Coherence;
Rotate {{0, 1, 0}, {0, 0, 0}, Pi/4} {
  Volume{75, 66};
}
Rotate {{0, 1, 0}, {0, 0, 0}, Pi/4} {
  Volume{75, 66};
}
Rotate {{0, 0, 1}, {0, 0, 0}, -Pi/2} {
  Volume{75, 66};
}
Translate {0, 0, -20} {
  Point{1, 2, 5, 10, 6, 7};
}
Translate {0, 10, 0} {
  Point{5, 10};
}
Translate {10, 0, 0} {
  Point{2, 7};
}
