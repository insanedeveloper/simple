#include <iostream>
#include <fstream>
#include <iomanip>
#include "solver.hpp"
#include <json/json.h>
#include <string.h>
#include "logger/logger.hpp"

#include <math.h>

#include "MeshSingle.hpp"

using namespace std;

string getPath(const char* filename);


int main (int argc, char* argv[]){


	Json::Value manifest;
	Json::Reader reader;
	char fake[] = "fakemanifest.json";
	char* filename = argc > 1? argv[1] : fake;

	string path = getPath(filename);
	LOG(INFO)<< "Current path is '" << path << "'";
	fstream file;
	file.open(filename, ios::in);
	bool parsingSuccessful = reader.parse( file, manifest );
	file.close();
	if ( !parsingSuccessful ){
	    // report to the user the failure and their locations in the document.
		 LOG(ERROR)  << "Failed to parse manifest '"<< filename <<"'\n"
	               << reader.getFormatedErrorMessages();
	    return 1;
	}

	Solver<MeshSingle>* s = new Solver<MeshSingle>(manifest, path);
	delete s;
	return 0;
}

string getPath(const char* filename){
    size_t l = strlen(filename) - 1;
    while(l > 0 && filename[l] != '/'){
        l--;
    }
    if(!l){
        return "./";
    }
    l++;
    char* str = new char[l];
    strncpy(str, filename, l);
    str[l] = '\0';
    string path(str);
    delete str;
    return path;
}
