#include "momentum.hpp"
#include <map>
#include <sstream>

using std::multimap;
using std::stringstream;

	size_t Momentum::vCount = 0;
	V* Momentum::dirVectors = 0;
	double* Momentum::dirWeights = 0;

	Momentum::Momentum(){
		if(!vCount){
			LOG(ERROR) << "Resize space first.";
			return;
		}
		values = new EnergyDistribution[vCount];
		for(size_t i = 0; i < vCount; i++){
			values[i] = 0;
		}
	}

	Momentum::Momentum(const Momentum& src){
		values = new EnergyDistribution[vCount];
		std::copy(src.values, src.values + vCount, this->values);
	}

	Momentum::~Momentum(){
		delete[] values;
	}
/**
 * TODO: test
 */
	int Momentum::findClosestFor(V& direction){
		double max = -1;
		int res = -1;
		for(size_t i = 0; i < Momentum::vCount; i++){
			double d = Momentum::dotProduct(i, direction);
			if(d > max){
				max = d;
				res = i;
			}
		}
		return res;
	}
	void Momentum::findTripleFor(V& direction, int* indexes, double* fractions){
		size_t maxVindex = 3;
		multimap<double,int> top;

		for(size_t i = 0; i < Momentum::vCount; i++){
			double d = Momentum::dotProduct(i, direction);
			cout<<direction<<" ," << Momentum::dirVectors[i] << " = " << Momentum::dotProduct(i, direction)<<endl;
			top.insert(pair<double,int>(d, i));
			if(i >= maxVindex){
				top.erase(top.begin());
			}
		}

		int i = 0;
		multimap<double,int>::iterator ptr = top.begin();
		multimap<double,int>::iterator end = top.end();
		while(ptr != end){
			fractions[i] = ptr->first;
			indexes[i] = ptr->second;
			ptr++;
			i++;
		}
		for(size_t i = 0; i < maxVindex; i++){
					cout<<indexes[i] <<": "<<fractions[i]<<endl;
				}
		V& a = Momentum::dirVectors[indexes[0]];
		V& b = Momentum::dirVectors[indexes[1]];
		V& c = Momentum::dirVectors[indexes[2]];
		double factor = 0;
		factor = det(a,b,c);
		if(factor == 0){
			LOG(ERROR) << "Bad vectors have been choosen as basis";
		}
		if(factor < 1E-5){
			LOG(WARNING) << "Possibly had a stiff problem while trying to find triple";
		}
		fractions[0] = det(direction, b, c)/factor;
		fractions[1] = det(a, direction, c)/factor;
		fractions[2] = det(a, b, direction)/factor;
		for(size_t i = 0; i < maxVindex; i++){
			cout<<indexes[i] <<": "<<fractions[i]<<endl;
		}
	}

	void Momentum::operator +=(const Momentum& d){
		EnergyDistribution* ptr = values;
		EnergyDistribution* dPtr = d.values;
		EnergyDistribution* end = values + vCount;
		while(ptr != end){
			*ptr++ += *dPtr++;
		}
	}
	void Momentum::operator +=(const EnergyDistribution& part){
	    for(size_t i = 0; i < vCount; i++){
			values[i].multAndAdd(part, dirWeights[i]);
		}

	}
	/* умножить в каждой группе значения на соответствующие множители в массиве */
	void Momentum::operator *=(const EnergyDistribution& factors){
		for(size_t i = 0; i < vCount; i++){
			values[i] *= factors;
		}
	}
	/*Momentum& Momentum::operator = (Momentum src){
		swap(*this, src);
		return *this;
	}*/
	Momentum& Momentum::operator = (Momentum& src){
		for(size_t i = 0; i < vCount; i++){
			values[i].equalize(src.values[i]);
		}
		return *this;
	}
	void Momentum::clear(){
		for(size_t i = 0; i < vCount; i++){
			values[i] = 0;
		}
	}
	bool Momentum::operator == (double value){
		EnergyDistribution* ptr = values;
		EnergyDistribution* end = values + vCount;
		while(ptr != end){
			if(*ptr++ != value){
				return false;
			}
		}
		return true;
	}
	EnergyDistribution Momentum::getFlow(){
		EnergyDistribution res = 0;
		for(size_t i = 0; i < vCount; i++){
			res.multAndAdd(values[i], dirWeights[i]);
		}
		return res;
	}

	EnergyDistribution Momentum::getVectorFlowIn(V& dir){
		EnergyDistribution res = 0;

		for(size_t i = 0; i < vCount; i++){
			EnergyDistribution& vals = this->getGroupForVector(i);
			double dot = dotProduct(i, dir);
			if(dot > 0){
				res.multAndAdd(vals, dirWeights[i] * dot);
			}

		}
		return res;
	}

	Json::Value Momentum::toJson(){
		Json::Value res(Json::objectValue);

		for(size_t i = 0; i < vCount; i++){
			stringstream str;
			str << Momentum::getVector(i);
			res[str.str()] = values[i].toJson();
		}
		return res;
	}

	void Momentum::staticFlush(){

		if(Momentum::dirVectors){
			delete[] Momentum::dirVectors;
		}
		if(Momentum::dirWeights){
			delete[] Momentum::dirWeights;
		}
		Momentum::vCount = 0;
	}

	double Momentum::capArea(double capAngle){
		double sinHalfTheta = sin(capAngle/2);
		return 4 * M_PI * sinHalfTheta * sinHalfTheta;
	}
	double Momentum::capAngle(double area){
		return 2*asin(sqrt(area/M_PI)/2);
	}

	void Momentum::resizeSpaceTo(Json::Value vectors){
		Momentum::vCount = vectors.size();
		Json::Value vector;
		Momentum::dirVectors = new V[vCount];
		Momentum::dirWeights = new double[vCount];
		double weights = 0;

		for(int i = 0; i < vCount; i++){
			Json::Value& vector = vectors[i];
			V vec(vector[0].asDouble(), vector[1].asDouble(), vector[2].asDouble());
			Momentum::dirVectors[i] = normalize(vec);
			LOG(INFO)<< Momentum::dirVectors[i];
			Momentum::dirWeights[i] = vector[3].asDouble();
			weights += Momentum::dirWeights[i];
		}
		if(weights != 1){
			LOG(WARNING) << "Weights sum is not 1 (" << weights << "), rescaling";
			for(size_t i = 0; i < vCount; i++){
				Momentum::dirWeights[i] /= weights;
			}
		}
	}

	void Momentum::resizeSpaceTo(size_t numVectors){
		Momentum::staticFlush();
		Momentum::vCount = numVectors;
		/* d = 2, σ = 4π, ν = 4π*sin(θ/2)*sin(θ/2) */
		double segmentSquare = 4 * M_PI / numVectors;
		double capAngle = Momentum::capAngle(segmentSquare);
		double dS = 1.0 / numVectors;

		/* Determining an ideal collar angle */
		double idealColarAngle = sqrt(segmentSquare);

		/* Determining the number of collars */
		double idealNumberOfCollars = (M_PI - 2*capAngle)/idealColarAngle;
		size_t numberOfCollars = fmax(1, floor(idealNumberOfCollars + 0.5));

		/* List of the ideal number of regions in each collar
		 * Number the zones southforward from 0 for the North pole cap to n for The South polar cap,
		 * and number the collars so that collar i is zone i+1 */
		double fittingCollarAngle = idealNumberOfCollars * idealColarAngle / (double)numberOfCollars;
		double* fittingColatitudes = new double[numberOfCollars+1];

		for(size_t i = 0; i <= numberOfCollars; i++){
			fittingColatitudes[i] = capAngle + i * fittingCollarAngle;
		}

		double* idealNumberOfRegions = new double[numberOfCollars];
		for(size_t i = 0; i < numberOfCollars; i++){
			idealNumberOfRegions[i] = (Momentum::capArea(fittingColatitudes[i+1]) - Momentum::capArea(fittingColatitudes[i]))/segmentSquare;
		}
		/* List of actual number of regions in each collar */
		int* numberOfRegions = new int[numberOfCollars];

		for(size_t i = 0; i < numberOfCollars; i++){
			double residual = 0;
			for(size_t j = 0; j < i; j++){
				residual += idealNumberOfRegions[j] - numberOfRegions[j];
			}
			numberOfRegions[i] = floor(idealNumberOfRegions[i] + residual + 0.5);
		}

		/* List of colatitudes */
		double* colatitudes = new double[numberOfCollars + 1];
		for(size_t i = 0; i <= numberOfCollars; i++){
			int segmentCount = 1;
			for(size_t j = 0; j < i; j++){
				segmentCount += numberOfRegions[j];
			}
			colatitudes[i] = Momentum::capAngle( segmentCount * segmentSquare );

		}
		/* Create list of vectors and everything else */
		Momentum::dirVectors = new V[numVectors];
		Momentum::dirWeights = new double[numVectors];
		/* North polar cap */
		Momentum::dirVectors[0] = V(0,0,1);

		/* South polar cap */
		Momentum::dirVectors[numVectors - 1] = V(0,0,-1);

		V* dirVectorPtr = Momentum::dirVectors + 1;

		for(size_t i = 1; i <= numberOfCollars; i++){
			double theta = (colatitudes[i] + colatitudes[i-1])/2;
			/* середина кольца */
			double segmentAngle = 2 * M_PI / numberOfRegions[i-1];
			for(int j = 0; j < numberOfRegions[i-1]; j++){
				double phi = segmentAngle * (j + 0.5);
				if(i > (numberOfCollars + 1)/2){
					/* это нужно, чтобы была центральная симметрия */
					phi *= -1;
				}
				*dirVectorPtr = V(cos(phi), sin(phi), cos(theta));
				dirVectorPtr++;
			}
		}
		for(size_t i = 0; i < numVectors; i++){
			dirWeights[i] = dS;
		}

		delete[] fittingColatitudes;
		delete[] idealNumberOfRegions;
		delete[] numberOfRegions;
		delete[] colatitudes;
	}
	void swap(Momentum& first, Momentum& second){
		std::swap(first.values, second.values);
	}
	bool Momentum::isNaN(){
		for(size_t i = 0; i < vCount; i++){
			if(values[i].isNaN()){
				return true;
			}
		}
		return false;
	}

	vector<V> Momentum::getVectorFlow(){
		size_t size = EnergyDistribution::getGroupCount();
		vector<V> flux;
		flux.resize(size);
		for(size_t v = 0; v < vCount; v++){
			EnergyDistribution& vals = this->getGroupForVector(v);
			for(size_t g = 0; g < size; g++){
				flux[g] += dirWeights[v] * dirVectors[v] * vals[g];
			}
		}
		return flux;
	}
