#ifndef SOURCE345
#define SOURCE345

#include "momentum.hpp"
#include <json/json.h>

class Source{
public:
	Source(std::string name, Json::Value& src);
	~Source();
	inline Momentum& getDistr(){
		return distr;
	}
	static void load(Json::Value& data);
	static Source* getByName(std::string name);
protected:
	friend class SourceTest;
	Json::Value loadSpectrumFromFile(std::string filename);
	Momentum distr;
	std::string id;
	static map<std::string, Source*> srcList;
};

#endif
