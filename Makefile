CXX      :=  g++
CXXFLAGS :=  -pg -O3 -Wall -MD -march=native # -fprofile-use

SRC_DIRS = materials mesh logger base64 singleton reactions

SRC = $(foreach dir,$(SRC_DIRS),$(shell find $(dir) -name "*.cpp")) \
    momentum.cpp\
    energyDistribution.cpp\
    MeshSingle.cpp\
    Transfer1.cpp\
    source.cpp\
    collider.cpp\
    stat.cpp \
    Throttle.cpp
MAIN_SRC = $(SRC) main.cpp
TEST_SRC = $(SRC) tests/main.cpp

MAIN_OBJ = $(MAIN_SRC:.cpp=.o)
TEST_OBJ = $(TEST_SRC:.cpp=.o)

HEADERS = solver.hpp momentum.hpp energyDistribution.hpp\
    $(foreach dir,$(SRC_DIRS),$(shell find $(dir) -name "*.hpp")) \
	Mesh.hpp\
    MeshSingle.hpp\
	typedef.hpp \
    Transfer.hpp\
    Transfer1.hpp\
	source.hpp\
	collider.hpp\
	stat.hpp \
    Throttle.hpp
	
TESTS := tests/*.hpp

.PHONY: jsoncpp jsoncpp-docs clean-jsoncpp sm
JSONCPP_DIR := jsoncpp
JSONCPP_H := $(JSONCPP_DIR)/dist/json/json.h
JSONCPP_S := $(JSONCPP_DIR)/dist/jsoncpp.cpp

HEADERS += $(JSONCPP_H)
SRC += $(JSONCPP_S)

JSONCPP_SRC = $(shell find $(JSONCPP_DIR) \( -name "*.h" -o -name "*.cpp" \) -not -path "$(JSONCPP_DIR)/dist/*")

.PHONY: all
all: simple

$(JSONCPP_H): $(JSONCPP_SRC)
	cd $(JSONCPP_DIR) && python ./amalgamate.py

jsoncpp-docs:	
	cd $(JSONCPP_DIR) && python doxybuild.py --doxygen=$(which doxygen) --with-dot	

clean-jsoncpp:
	rm -rf $(JSONCPP_DIR)/dist

sm:
	git submodule update --init

simple: $(MAIN_OBJ)
	$(CXX) -o simple $(MAIN_OBJ) $(CXXFLAGS)


tests: $(TEST_OBJ) $(TESTS)
	$(CXX) -o tests/test $(TEST_OBJ) $(CXXFLAGS)
	@echo "Running tests...\n"
	time ./tests/test

tests/main.o: tests/main.cpp tests/*.hpp
main.o: solver.hpp

%.o: %.cpp | $(JSONCPP_H)
	$(CXX) $(CXXFLAGS) -c $< -o $@ -I $(JSONCPP_DIR)/dist

.PHONY: obj
obj: $(MAIN_OBJ) $(TEST_OBJ)

.PHONY: clean
clean:
	find . -not -ipath "$(JSONCPP_DIR)/*" -iname "*.[od]" -exec rm '{}' ';'
	rm -rf simple

include $(shell find . -name "*.d")
