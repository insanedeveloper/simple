#include "stat.hpp"
#include <fstream>
#include "auxiliary.hpp"

using namespace Json;

Stat::Stat(Json::Value& data){
	rate = data["rate"].asInt();
	file = data["file"].asString();
	if(data.isMember("dirs")){
		Value& vectors = data["dirs"];
		int l = vectors.size();
		LOG(INFO) << "Stat: vectors found:" << l;
		this->flowDirs.resize(l);
		for(int i = 0; i < l; i++){
			Value& vector = vectors[i];
			V v = normalize(V(vector[0].asDouble(), vector[1].asDouble(), vector[2].asDouble()));
			this->flowDirs[i] = v;
		}
	}
}
void Stat::saveEnergyDistribution(int cellID, std::string prefix, EnergyDistribution& part){
	if(!snapshotI){
		return;
	}
	std::string idStr = toStr(cellID);
	if(!data.isMember(idStr)){
		data[idStr] = Json::Value(objectValue);
	}
	data[idStr][prefix] = part.toJson();
}
void Stat::saveDouble(int cellID, std::string name, double value){
	if(!snapshotI){
		return;
	}
	std::string idStr = toStr(cellID);
	if(!data.isMember(idStr)){
		data[idStr] = Json::Value(objectValue);
	}
	data[idStr][name] = value;
}
void Stat::collectDataAndPrint(Mesh& mesh){
	if(!snapshotI){
		return;
	}
	data["i"] = snapshotI;
	for (unsigned int j = 0; j < mesh.getMyCells().size(); j++){
		std::string cellID = toStr(mesh.getMyCellIndexes()[j]);
		if(!data.isMember(cellID)){
			data[cellID] = Json::Value(objectValue);
		}
		Value& cellData = data[cellID];
		Polygon* cell = mesh.getMyCells()[j];

		cellData["id"] = mesh.getMyCellIndexes()[j];
		cellData["f"] = cell->f().getFlow().toJson();
		cellData["density"] = cell->getDensity();

		//cellData["f_vec"] = cell->f().toJson();
		/*cellData["vectorflow"] = this->fluxToJson(cell->f().getVectorFlow());*/

		if(this->flowDirs.size() > 0){
			for(size_t i = 0; i < this->flowDirs.size(); i++){
				stringstream str;
				str << "vectorflow@" << this->flowDirs[i];
				cellData[str.str()] = cell->f().getVectorFlowIn(this->flowDirs[i]).toJson();
			}
		}
		Material* mat = cell->getMaterial();
		if(mat){
			cellData["material"] = mat->getId();
		}
	}
	printData(mesh);
}
void Stat::printData(Mesh& mesh){
	std::ofstream dataFile;

    int rank = mesh.getRank();
    std::string macroFileName = file;
    macroFileName += "-" + toStr(rank) + "-" + toStr(snapshotI) + ".json";

    dataFile.open(macroFileName.c_str(), std::ios::out);
    dataFile << data;
    dataFile.close();

    mesh.barrier();
    turnOff();
}

Json::Value Stat::fluxToJson(vector<V> flux){
	size_t size = flux.size();
	Json::Value res(objectValue);
	for(size_t i = 0; i < size; i++){
		Json::Value groups(arrayValue);
		V& f = flux[i];
		groups[0] = f[0];
		groups[1] = f[1];
		groups[2] = f[2];
		res[toStr(i)] = groups;
	}
	return res;
}
