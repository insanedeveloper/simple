#ifndef ENERGYDISTR34532
#define ENERGYDISTR34532

#include <limits>
#include <json/json.h>
#include "auxiliary.hpp"
#include "logger/logger.hpp"
enum scaleType {
	LINEAR = 0,
	LOGARITHMIC = 1
};
#define NEUTRON_MASS 1.674927352E-24 /* g */
#define EL_CHARGE 1.602176565E-19/* C */

class EnergyDistribution{
public:
	static void setResolution(size_t N, double min, double max, scaleType type);
	static void setCourantNumber(double courant);

	static EnergyDistribution toEnergyGroups(double* erg, double* val, size_t count);
	static EnergyDistribution toEnergyGroups(Json::Value& array);
	static std::string getSpaceInfo();
	inline static double* getGroups(){
		return eGroups;
	}
	inline static size_t getGroupCount(){
		return EnergyDistribution::gCount;
	}
	friend std::ostream& operator<< (std::ostream& to, EnergyDistribution& a);

	EnergyDistribution();
	EnergyDistribution(const EnergyDistribution& src);
	EnergyDistribution(double value);
	~EnergyDistribution();
	void trimNegative();
	EnergyDistribution operator * (const EnergyDistribution& src);
	EnergyDistribution operator / (const EnergyDistribution& src);
	EnergyDistribution operator + (const EnergyDistribution& src);
	EnergyDistribution operator - (const EnergyDistribution& src);
	EnergyDistribution operator * (double mult);
	void operator *= (const EnergyDistribution& src);
	void operator /= (const EnergyDistribution& src);
	void operator *= (double mult);
	void operator /= (double mult);
	void operator += (const EnergyDistribution& src);
	void operator -= (const EnergyDistribution& src);
	EnergyDistribution& equalize (EnergyDistribution& src);
	EnergyDistribution& operator = (EnergyDistribution src);
	EnergyDistribution& operator = (double value);
	bool operator ==(double value);
	inline bool operator !=(double value){
		return !(*this == value);
	}
	/**
	 * Каждую группу из src равномерно размазывает по группам, не превосходящим данную.
	 */
	EnergyDistribution spreadDown();
	/* растягивает распределение по энергии в mult раз */
	EnergyDistribution stretch(double mult);
	void multAndAdd(const EnergyDistribution& src, double k);
	void multAndSub(const EnergyDistribution& src, double k);
	void multAndAdd(const EnergyDistribution& src0, const EnergyDistribution& src1);
	void multAndSub(const EnergyDistribution& src0, const EnergyDistribution& src1);

	Json::Value toJson();
	double totalFlow();
	bool isNaN();
	double& operator [](size_t index);
private:
	friend class EnergyDistributionTest;
	static size_t gCount;
	static scaleType interpolationType;
	static double step;
	static double courantNumber;
	static double* eGroups;
	static double* width;
	static double* avgEinGroup;
	static inline double interpolate(double x, double beginX, double endX, double beginY, double endY){
		double k = (endY - beginY) / (endX - beginX);
		if(x < beginX){
			LOG(ERROR) << "out of region:" << x << "<" << beginX;
		}
		if(x > endX){
			LOG(ERROR) << "out of region:" << x << ">" << endX;
		}
		if(endX - beginX == 0){
			LOG(ERROR) << "zero width in interpolation " << x << " from " << beginX << " to " << endX;
		}
		return beginY + k*( x - beginX );
    }
	static inline double sum(double beginX, double endX, double beginY, double endY){
		/* результат нужно делить на 2 */
	    return (beginY + endY)*(endX - beginX);
	}
	static void staticFlush();
	static void getLinGroupAndRatioFor(double E, int& index, double& ratio);
	static void getExpGroupAndRatioFor(double E, int& index, double& ratio);
	inline void allocValues(){
		if(gCount){
			values = new double[gCount];
		}else{
			LOG(ERROR) << "Attempt to create EnergyDistribution before setting resolution";
		}
	}
	double getInterpolatedByEnergy(double E);
	friend void swap(EnergyDistribution& first, EnergyDistribution& second);
	double* values;
};
#endif
